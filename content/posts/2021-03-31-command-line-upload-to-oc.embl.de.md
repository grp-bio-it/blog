---
date: 2021-03-31
description:
featured_image: "images/2021-03-31/oc.embl.de.png"
tags: ["Renato Alves", "Bio-IT", "Owncloud", "Command-line"]
title: "Uploading to ownCloud (@EMBL) from command-line"
omit_header_text: true
---

EMBL provides an [ownCloud][owncloud] server with (as of March 2021) 50GiB of storage for every EMBL user.
To access it navigate to [oc.embl.de][ocembl] and login using your EMBL credentials.

## An alternative to Google Docs/Drive

You can use this system much like [Google Docs or Google Drive][oc-google-howto] by storing and sharing files or using the built in tools to edit documents.

## Programmatic access

Besides public and private links for download and upload,
Owncloud provides a [WebDAV][webdav] interface that can be used for programmatic access.

You can find the WebDAV URL for your user by logging in to [oc.embl.de][ocembl] and clicking the **Settings** option in the bottom left corner of the screen.

![WebDAV URL](../../images/2021-03-31/webdav-url.png)

### Programmatic upload

Lets assume we have a file called `hello.txt` that we want to upload to a folder called `uploads` in your [oc.embl.de][ocembl] account.

We can start by creating the test file.
```
echo 'Hello Owncloud!' > hello.txt
```

As we will need to use your account password, we can use the `read` command to type it without accidentally adding it to the shell history.
Execute the next command, type your password and press Enter/Return.
```
read -s PASSWORD
```

You can confirm that the command worked by executing the next command that will display your password on screen:
```
echo $PASSWORD
```

In order to upload the file into a `uploads` folder we must first create the folder.
This can be achieved using the `MKCOL` instruction using `curl` and your WebDAV URL.
```
curl -X MKCOL -u username:$PASSWORD "https://oc.embl.de/remote.php/dav/files/.../uploads"
```
where `username` should be your EMBL username,
`...` should be a unique identifier part of the WebDAV URL you copied above
and the `/uploads` part indicates the folder name we want to create.

If you received an error in this step, verify that your username, password and WebDAV URL are correct.

Once successful, if you refresh your browser you should now see an `uploads` folder.

We are now ready to upload our `hello.txt` file which we can do with:
```
curl -X PUT -u username:$PASSWORD "https://oc.embl.de/remote.php/dav/files/.../uploads/hello-in-owncloud.txt" --data-binary @"hello.txt"
```
notice that we now use a `PUT` instruction instead of `MKCOL`,
the URL ends in `uploads/hello-in-owncloud.txt` to indicate the folder and filename we want to create
and `@"hello.txt"` indicates the name of the file in your computer.

If successful you should now see a `hello-in-owncloud.txt` file inside the `uploads` folder when visiting [oc.embl.de][ocembl].

Finally to clear the password stored in `$PASSWORD` simply close the shell or execute:
```
unset PASSWORD
```

### Additional WebDAV instructions

Besides `MKCOL` and `PUT`, WebDAV supports `MOVE` and `PROPFIND` instructions.
You can read more about these in the [ownCloud official documentation][ocdocs].


[owncloud]: https://owncloud.com
[ocembl]: https://oc.embl.de
[bio-it]: https://bio-it.embl.de
[bio-it-mail]: mailto:bio-it@embl.de
[oc-google-howto]: https://bio-it.embl.de/how-to-use-embl-owncloud-as-alternative-to-google-docs-drive/
[webdav]: https://en.wikipedia.org/wiki/WebDAV
[ocdocs]: https://doc.owncloud.com/server/user_manual/files/access_webdav.html#accessing-files-using-curl
