---
date: 2021-01-29
description:
featured_image: "images/2021-01-29/00-embl_mattermost.png"
tags: ["Jelle Scholtalbers", "Renato Alves", "Bio-IT", "Community", "Mattermost", "GitLab", "Chat"]
title: "The EMBL chat"
omit_header_text: true
---

[EMBL's (Bio-IT) chat][embl-chat] has been initiated in 2017 to facilitate internal real-time interactions between people at EMBL, to promote group discussions and sharing of information.

As of 2021 more than 1000 users are registered with over 120 active daily.
In part due to the 2020 SARS-CoV-2 pandemic situation, the platform gained particular relevance and became a central hub for communication between members of all EMBL sites.
The platform effectively became a pan-EMBL service in 2021, welcoming EMBL-EBI, that would join all other EMBL sites.


## Sounds great! How do I to join?

If you are an active EMBL employee, you can access https://chat.embl.org and login with your EMBL credentials by following the instructions below.

In order to welcome you to the [EMBL chat][embl-chat] we first need to authenticate you through EMBL's authentication portal.
This can all be a little confusing as there are a few pages to go through, so bear with us.

This guide will walk you though the entire process. Lets get started.


### What is Mattermost?

First, point your browser to [EMBL's chat (https://chat.embl.org)][embl-chat] and you will be greeted with:

![Mattermost landing page](../../images/2021-01-29/01-landing-page.png)

The EMBL chat is powered by an open-source platform called [Mattermost][mattermost].
If you are familiar with [SLACK][slack], this platform is very similar.
The main difference is that it is hosted at EMBL Heidelberg and includes channels and features tailored to help you while at EMBL.

In the current page you will see two options **View in Desktop App** and **View in browser**.
For now lets login via the browser. If at a later stage you prefer to use a [Mattermost App][mattermost-app], logging in there should be straightforward.

Please go ahead and click **View in Browser**.


### EMBL's chat login page

![EMBL mattermost login page](../../images/2021-01-29/02-login-via-gitlab.png)

The greeting message here tells you a little more about the chat platform and who maintains it.
It also mentions [Mattermost][mattermost], specifically GitLab Mattermost,
and a few channels that you will be welcome to join later.
Channels start with a tilde (`~`). This is a Mattermost convention.

By clicking the highlighted button you'll be directed to [EMBL's GitLab][embl-gitlab],
another pan-EMBL service managed by [Bio-IT][bio-it], that will take care of identifying you.
Clicking the highlighted **GitLab** button will redirect you to the GitLab login portal.


### Login via GitLab

![Login happens via git.embl.de](../../images/2021-01-29/03-gitlab-embl-ebi-login.png)

You may notice the different login possibilities: **"EMBL only"** and **EMBL & EBI**.
If located at EMBL-EBI, only the bottom option will recognize your credentials.
If you choose to use the **EMBL only** form, you can [skip to the two-factor authentication step](#two-factor-authentication).

To follow this guide, lets go ahead and click **Login with EMBL/EBI credentials (SSO)**.


### Authenticate to EMBL's SSO service

![Login via EMBL SSO](../../images/2021-01-29/04-sso-embl-login.png)

Here you should see a page that may be more familiar.
You should always be cautious when providing your EMBL credentials. Here you can confirm the address `idp.embl.de` which is EMBL's **ID**entification **P**ortal.

By providing your **EMBL username**, your **EMBL password**, selecting the **EMBL site** you are employed at, and clicking **Login**, you will login to GitLab through EMBL.


### Two-factor authentication

![Two-factor authentication on git.embl.de](../../images/2021-01-29/05-two-factor-auth.png)

If you see the above screen, you have enabled two-factor authentication on [EMBL's GitLab][embl-gitlab] platform.
Simply provide your one-time-password (OTP) 6 digit token to proceed.

If you see something different, [skip to the next step](#authorizing-chatemblorg).


### Authorizing chat.embl.org

![Authorize chat.embl.org](../../images/2021-01-29/06-authorize-embl-chat-in-gitlab.png)

Since we are using [EMBL GitLab][embl-gitlab] as identity provider to Mattermost, we need to allow **chat.embl.org** to access the GitLab account.

Hit **Authorize** to finalize the process.


### Joining the EMBL team

At the time of writing, there is only one public team, **EMBL**.
Other teams exist for different purposes but they can only be joined by invite.

If you followed a link to a channel, you will likely join the EMBL team automatically.
If you didn't, you may see the page below:

![Joining the EMBL team](../../images/2021-01-29/07-choose-embl-team.png)

Go ahead and click **EMBL** to join the team.

And you are **done**!

See also the next section if you'd like to learn which channels to join.

### Welcome message

When you first join the chat, you will be greeted by [WelcomeBot][welcomebot].

![WelcomeBot message](../../images/2021-01-29/08-welcomebot.png)

This list includes some of the most active channels.
If interested, in one or more of the listed topics, click the corresponding button(s) and you will be added to the respective channels.
For each topic selected, you will receive a message indicating the channels you have been added to and their description.

A list of some of the channels can also be found [in the "Where to find us on the Bio-IT chat" blog post][find-channels].


# Frequently Asked Questions

## What if I'm outside EMBL?

https://chat.embl.org is reachable from anywhere where you have an internet connection.


## Can I use the chat from my desktop, tablet or phone?

The [EMBL Bio-IT chat][embl-chat] is powered by an open-source chat platform called [Mattermost][mattermost].

The most common way to access the chat is through a browser (such as Firefox, Chrome, Safari, ...)
but native applications also exist for [Android, iOS and all major operating systems][mattermost-app]

As we are hosting the free open-source [community edition of Mattermost, also known as *Enterprise E0*][mattermost], **push notifications** on mobile apps are currently **not available**.


## How do I invite collaborators?

For privacy and confidentially reasons we limit access to EMBL employees and collaborators with an e-visitor account.

However, due to high demand, we now have an experimental technology that allow connecting spaces across platforms.
If your collaborators use [SLACK][slack] or any of [these platforms](https://github.com/42wim/matterbridge#natively-supported), please [email us](#who-should-i-contact-in-case-of-issues) and we'll do our best to assist.


## How private are private channels?

Private channels are only visible to members and can not be joined freely.
New members must be added manually by a channel administrator.

A creator of a channel automatically becomes it's administrator.
A channel administrator can grant other channel members admin status.


## I would like to discuss sensitive topics, how secure is the chat?

All communication through the chat follows
[IP 68](https://intranet.embl.de/hr/internal_policies/IP68-Data-Protection-EN-18052018.pdf) and
[IP 54](https://intranet.embl.de/hr/internal_policies/ip54_computeruse.pdf)
and is considered confidential (comparable to e-mail).

EMBL's chat server is hosted at EMBL Heidelberg on infrastructure provided by Heidelberg IT Services
and is [maintained by a small team](#who-should-i-contact-in-case-of-issues).

Communication with the Mattermost server uses TLS/SSL encryption which secures transit.
Fully end-to-end encrypted communication is not possible with this platform.

Conversations and attachments, be them in public, private channels or via direct communication,
are stored in Mattermost's database unless deleted by the sender or a server administrator.


## Can I delete a channel and all its content?

Deleting a channel and all its history is possible but not available through the Mattermost interface.
Please see the next question for whom to contact if this need arises.


## Can I have a team for my group/project?

Yes, this is allowed, however, having multiple teams can lead to a poor user experience.
Among different aspects, multiple teams can lead to, confusion, due to channels with similar names existing in multiple teams, and missing information, from teams other than the open team.

Our recommendation is to use the **EMBL team**, create as many public and/or private channels as necessary, and **prefix them with your team name**.
Users can also [create channel categories](https://docs.mattermost.com/channels/customize-your-channel-sidebar.html) and group the channels as desired.

This will ensure everyone can coexist without scattering EMBL's community across different teams or spaces.


## Who should I contact in case of issues?

[EMBL Bio-IT chat][embl-chat] is maintained by Jelle Scholtalbers and Renato Alves and support requests can be sent to [gitadmins@embl.de][gitadmins].


Jelle Scholtalbers and Renato Alves,  
EMBL's Chat team

[bio-it]: https://bio-it.embl.de
[embl-chat]: https://chat.embl.org
[embl-gitlab]: https://git.embl.de
[mattermost]: https://mattermost.com
[mattermost-app]: https://mattermost.com/apps/
[slack]: https://slack.com
[gitadmins]: mailto:gitadmins@embl.de
[welcomebot]: https://chat.embl.org/embl/messages/@welcomebot
[find-channels]: https://grp-bio-it.embl-community.io/blog/posts/2023-02-07-embl-chat-channels/#computational-research-and-data-management-support
