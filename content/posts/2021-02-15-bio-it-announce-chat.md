---
date: 2021-02-15
description:
featured_image: "images/2021-02-15/embl_mattermost_bot.png"
tags: ["Renato Alves", "Bio-IT", "Community", "Mattermost", "Chat", "Bio-IT-Bot"]
title: "Welcome Bio-IT bot and Bio-IT-announce channel"
omit_header_text: true
---

Initially proposed as an EMBL's coding club collaborative programming project, the [EMBL Bio-IT chat bot][bit-bot], avidly named `BIT`, took off in 2020
and is already diligently announcing courses and workshops in the [Bio-IT-announce][bio-it-announce] channel.

## Introducing BIT

With a modular design, allowing for anyone to contribute new behavior with very little code,
[`BIT`][bit-bot], EMBL's Bio-IT chat bot,
is slowly gaining new abilities and showing its usefulness.
As of 2021 it already
assists [EMBL's Git admin team][git-embl] on specific administrative tasks and
monitors EMBL and [Bio-IT services][bio-it] keeping us up to date with changes.

As it picks up momentum, new features are being added regularly and [more are on the way](https://git.embl.de/grp-bio-it/bio-it-bot/-/issues?label_name%5B%5D=feature).
If you are interested or would like to contribute with ideas or code, let us know in the [Bio-IT channel][bio-it]
or email [Bio-IT][bio-it-mail].

## Announcements in Bio-IT-announce channel

As mentioned above, the [Bio-IT-announce channel][bio-it-announce] has been created to
increase the visibility of courses and workshops offered at EMBL and by different external groups and communities.
On its way to 50 members in little over a month and combining the advantages of a *mailing list* and a *discussion forum*,
is already [delightfully fulfilling its purpose](https://chat.embl.org/embl/pl/y5j1q1aprfn95c6frz5jyqbjqe).

But we said *mailing list* so how do you go about using it as such?

*Mailing lists* typically have one of two options, daily email digests or individual emails for every message sent.
Unfortunately Mattermost doesn't yet support the daily digest option but it's possible to receive a notification every time a new message is posted.
As the channel is currently relatively low traffic, you can enable this option by navigating to **notification preferences**.

![Bio-IT Announce channel title](../../images/2021-02-15/channel_title.png)

and setting the notification level to **For all activity**

![Bio-IT Announce channel settings](../../images/2021-02-15/channel_settings.png)

which will ensure you receive an email when you are not active in the chat and as soon as announcements are made.

If in the future you find that notifications are too frequent or that you are sufficiently active in the chat that you don't need those extra emails,
you can always set it back to **Global default** or **Never**.

Let us know if you are finding [`BIT`][bit-bot]'s services useful and feel free to send suggestions for new features [our way][bio-it-mail].

# Disclaimer

Chat logo by [unDraw][undraw].

[bit-bot]: https://git.embl.de/grp-bio-it/bio-it-bot
[bio-it]: https://bio-it.embl.de
[bio-it-mail]: mailto:bio-it@embl.de
[bio-it-announce]: https://chat.embl.org/embl/channels/bio-it-announce
[git-embl]: https://git.embl.de
[undraw]: https://undraw.co
