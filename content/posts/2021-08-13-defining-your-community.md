---
date: 2021-08-13
description:
featured_image: "/images/2021-08-13/community.jpg"
tags: ["Lisanna Paladin", "Bio-IT", "Community", "CommunityRule"]
title: "How defining the characteristics of your community can make you more effective"
omit_header_text: true
---

"[CommunityRule](https://communityrule.info/) is a governance toolkit for great communities".
The website homepage provides this definition, and then challenges you with two questions:
- How does your community work?
- Are you ready to make hard decisions?

## Defining our governance structure

The purpose of the CommunityRule project is to make you think about how your community is
organised, who is in charge of making decisions and who can influence this decisions. The
project is still work in progress, but a wide range of definitions and tags is already
available - even if not all of them are thoroughly described - to define your community
governance structure. The entire project is open source and available in
[GitLab](https://gitlab.com/medlabboulder/communityrule). Well, we tried it.

The descriptive terms are organised in four groups:
- Culture :art:
- Decision :ballot_box_with_check:
- Process :arrows_counterclockwise:
- Structure :office:

And you should include in your governance structure some terms from each of these. You can start from scratch or choose a template. As most
[good programmers](https://levelup.gitconnected.com/a-good-programmer-is-a-lazy-programmer-8982b2d971bb), we are lazy -
and this is why we chose to start from a template.

![Templates](../../images/2021-08-13/Templates_Comm.png)

Among those available, can you guess which one captured our attention?

## The Do-ocracy

As simply as it sounds, the do-ocracy is a community where **those who take initiative to do
something can decide how they do it**. With few variations with respect to the provided
template, here's our proposal for the Bio-IT governance structure.

**Disclaimer**: This is to be considered a proposal and indeed the purpose of this blog post is to
initiate a discussion about it.

_____

### Bio-IT governance structure

* **Values** :art: Bias for action, consultation, decentralization
    * **Mission** :art: A shared purpose is clearly defined.
* **Autonomy** :ballot_box_with_check: There are no fixed structures. Participants can organize meetings or sub-groups as they see fit.
* **Do-ocracy** :ballot_box_with_check: Participants are responsible for carrying out their own initiatives, optionally in collaboration with others.
    * **Transparency** :arrows_counterclockwise: Important information is widely available.
    * **Lobbying** :arrows_counterclockwise: If someone has serious concerns about an initiative, they should raise those concerns with the person responsible for it.
* **Caucus** :office: A semi-formal group of people organized around shared priorities.

_____

This description is focused on two main points: 1) our community is united by a common aim, that is to promote a **culture
of computational best practices at EMBL**, and 2) we value and support **independent initiative, provided transparency**.
As regards the organisational values and desired behaviour of Bio-IT members, the project adheres to EMBL
code of conduct in [training](https://www.embl.de/training/events/info_participants/codeofconduct/) and
[research](http://www.embl-hamburg.de/aboutus/communication_outreach/media_relations/2006/061214_brussels/index.html).
Specific issues on a Bio-IT activity should be discussed with the people mostly involved in its organization. Bio-IT
project managers [Renato Alves](https://bio-it.embl.de/renato-alves/) and
[Lisanna Paladin](https://bio-it.embl.de/lisanna-paladin/) can act as facilitators and moderators of this discussion.

### The governance structure impact

You would be surprised by how such a small document can impact your way of working. Since we drafted and discussed it,
we felt empowered to **do** things accordingly. It started with some jokes in the
[chat](https://chat.embl.org/embl/channels/bio-it), like "since this is a do-ocracy, I will start doing this" or
"I'll inform you later - when this is clearer in my mind - first I'll start experimenting this". These first sentences
gave structure and definition to what was our spontaneous way of working, the difference being that this was agreed.
This is exactly the reason why this blog plost: we would like to share :arrow_right: discuss :arrow_right: agree on this
description with you, to make you feel empowered to [get involved](https://bio-it.embl.de/get-involved/) in Bio-IT
activities. **What do you think?**

