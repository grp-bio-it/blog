---
date: 2022-08-31
description:
featured_image: "/images/2022-08-31/chris-montgomery-smgTvepind4-unsplash.jpg"
tags: ["Lisanna Paladin", "Bio-IT", "BioNT", "training"]
title: "The Bio Network for Training (BioNT)"
omit_header_text: true
---

**Visit the official BioNT website: [biont-training.eu](http://biont-training.eu/)**

# Short term training courses, DIGITAL Europe

The [DIGITAL Europe Programme](https://digital-strategy.ec.europa.eu/en/activities/digital-programme)
call [DIGITAL-2022-TRAINING-02-SHORT-COURSES](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/digital-2022-training-02-short-courses)
aims at increasing the training offer in digital skills of people in the workforce and job-seekers, through free and open
short-term courses. Bio-IT, leading a consortium of 9 institutions in 4 countries, applied to the call with a project for the computational
training of people in the Life Sciences sector. The project got funded and will start its activities in early 2023. 

# The project: BioNT

**BioNT - Bio Network for Training -** is an international consortium of **nine 
academic entities and SMEs** with the aim to provide a high-quality 
training program for **digital skills relevant to the biotechnology 
industry and biomedical sector**. 
- The **basic curriculum** of the program 
aims at employees or job-seekers with limited prior experience in 
handling, processing and visualising biological data and/or the 
usage of computational biology tools. 
- The **advanced curriculum** is 
targeted at Data Stewards, System Administrators, and other 
professionals who hold leading roles in the management and 
deployment of computational resources at their workplaces. 

The curriculums will include four courses each. The basic curriculum will be delivered twice, and the advanced 
curriculum once during the three years of the project. All courses will be offered online, with the possibility to 
request for or train a local helpers for companies or institutions which will be interested in registering a local 
hub of trainees in person. In addition, BioNT community will meet in occasion of the Community Event. 

The list of project partners includes, as training providers:
- EMBL, the European Molecular Biology Laboratory (Germany)
- ZB MED, the Deutsche Zentralbibliotek Medizin (Germany)
- EPFL, the École polytechnique fédérale de Lausanne (Switzerland)
- the University of Barcelona (Spain)
- ALU-FR, the Albert-Ludwigs-Universität Freiburg (Germany)
- the University of Oslo (Norway)

and three SMEs (Small or Medium Enterprises):
- Simula Learning (Norway)
- BioByte (Germany)
- HPCNow! (Spain)

The consortium will act in synergy with wider initiatives at EMBL and beyond: internally with the
[Data Sciences programme](https://www.embl.org/about/programme/data-sciences-plans/), 
bioinformatics and computational biology networks such as [ELIXIR](https://elixir-europe.org/) and the German 
node [de.NBI](https://www.denbi.de/), as well as training communities such as [The Carpentries](https://carpentries.org/), 
[CodeRefinery](https://coderefinery.org/), and [Open Life Sciences](https://openlifesci.org/). 

BioNT will play a key role in shaping a specific branch of Bio-IT's training programme, delivering and organising 
courses in fundamental skills for the Life Science industry. A project manager position will be open soon at EMBL within 
this initiative. Write to us at [bio-it@embl.de](mailto:bio-it@embl.de) for further information about the project.

*Photo by [cwmonty](https://unsplash.com/@cwmonty) on [Unsplash](https://unsplash.com/)*
