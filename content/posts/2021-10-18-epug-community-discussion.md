---
date: 2021-10-18
description:
featured_image: "/images/2021-10-18/python-post-it.jpg"
tags: ["Lisanna Paladin", "Bio-IT", "EPUG", "Python", "community"]
title: "A community discussion about the EMBL Python User Group"
omit_header_text: true
---

The [**EMBL Python User Group (EPUG)**](https://bio-it.embl.de/embl-python-user-group/) is a collective of python programmers at EMBL 
who meet regularly (every other Tuesday at 4 PM) to discuss python-related topics, demonstrate tools and modules, and 
experiment with python-related software and hardware.

After the summer break, we reached out to the community to discuss EPUG format, addressed level of expertise, ideas for
improvements and answer potential newcomers questions. This post includes a summary of what happened.

## Step one: the community discussion

On the 28th of September we reserved an EPUG meeting not to talk about Python, but to talk about the Python community
itself. We wanted to take the time to think about how we are managing the community interactions, to question the 
format of EPUG meetings "as we have been doing them in the past" and ask ourselves:
- Are there other possible **formats** for EPUG meetings?
- Which is our target **level of expertise** currently, and do we want to change it?
- What are the **topics** we mostly discuss, and do we want a different focus?
- What is our **motivation** to join EPUG? Can we draw on that to advertise it?
- Is anyone else interested in joining or having a **coordination role** in the community?

The most wide-interest outcome of this discussion is a **table comparing the possible formats** for the 
coding group, that we report here. We believe this can serve as a guide to re-discuss or initiate similar coding
groups in any programming language.

| Format               | Pros                                      | Cons                                                                          | Challenges                                                                    |
| -------------------- | ----------------------------------------- | ----------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| Independent topics   | Loose sessions                            | Unstructured                                                                  | Selection of topics / session leads                                           |
| Book Club            | Structured curriculum                     | Increasing level / Pre-requisites / Reduced attendance in time                | Selection of leads                                                            |
| Video Club           | Discussion starter                        | If watching videos before: Homework ; If watching videos during: Low interest | Selecting videos relevant to many, identify good videos without watching them |
| Collaborative coding | Excellent learning experience             | Different styles and approaches to problems                                   | Finding a common problem/project, Level asymmetry                             |
| Code review          | Very helpful, increasing expert knowledge | Increased required time                                                       | Review depth depends on time spent reviewing and code complexity              |
| Bring your problem   | Focused discussion, targeted and relevant | Low bandwidth, single person, better suited for Bio-IT Drop-in?               | Problems need to be well defined and approachable in one session              |

The current format of EPUG is the one listed first, called **Independent topics**. Topics discussed range from beginner 
to advanced materials (with a preference for intermediate/advanced). For reference, they are stored in the 
[EPUG repository](https://git.embl.de/grp-bio-it/EPUG). They are also often followed up with discussions in 
the [Python channel in EMBL's chat](https://chat.embl.org/embl/channels/python). Sessions are planned in a 
[shared document](https://docs.google.com/spreadsheets/d/1cMDumZ8GdhsTaiWzJl1qHDfi192sl4P1KJJhtuk0ZkE/edit?usp=sharing).

## Step two: the survey

The community discussion served as a basis to structure a survey, that was circulated among EPUG participants. Detailed 
results of the survey are available at the [EPUG repository](https://git.embl.de/grp-bio-it/EPUG), here some highlights.

| Summary                                                                                                                                                                    | Plot                                         |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| 1. The session level we prefer is **intermediate/advanced**.                                                                                                               | ![levels](../../images/2021-10-18/levels.png)   |
| 2. The format we prefer is **independent topics**, although we could experiment others too                                                                                 | ![formats](../../images/2021-10-18/formats.png) |
| 3. Our topics preference is quite distributed, we should take care of covering all of them                                                                                 | ![topics](../../images/2021-10-18/topics.png)   |
| 4. When we asked "If you never joined EPUG until now, why is it so?" the main answers were either because of the too specific topic selection or because of available time | ![why-not](../../images/2021-10-18/why-not.png) |

Through the survey, we also learned that the current time slot works well, and we recruited a couple new 
coordinators for the group (announcement coming soon in the [chat channel](https://chat.embl.org/embl/channels/python)
)! 

## So, what's the plan now? 

We will keep the current time slot: **every other Tuesday at 4 PM**.

We will keep the current format: **independent topics**. However, we will encourage session leaders to experiment 
other formats if they feel so. 

We will enrich the **description of each session** with motivation, potential fields of application and possibly the 
topic keywords reported from the survey. Our priorities will be to make the session announcements newcomers-friendly, 
and to select topics with a wide range of applications.

EPUG is a friendly group, our meetings are great way to get to know other people at EMBL who use Python, i.e. potential
helpers, consultants, collaborators. **Join us through**:
- Joining the mailing list [python@embl.de](mailto:python@embl.de) by emailing [bio-it@embl.de](mailto:bio-it@embl.de)
- Checking the [schedule](https://docs.google.com/spreadsheets/d/1cMDumZ8GdhsTaiWzJl1qHDfi192sl4P1KJJhtuk0ZkE/edit#gid=0) 
and joining next virtual session in the [zoom room](https://www.google.com/url?q=https://zoom.us/j/96003226281?pwd%3DKzZLemNmU1p4OHRVeGJuaFE2WEhYdz09&sa=D&source=calendar&ust=1634797428144905&usg=AOvVaw3Of6mjuVTXzzAXIL4bzEsP)

We are looking forward to meet you!