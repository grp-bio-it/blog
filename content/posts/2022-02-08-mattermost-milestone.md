---
date: 2022-02-08
description:
featured_image: "images/2022-02-08/ray-hennessy-gdTxVSAE5sk-unsplash.jpg"
tags: ["Renato Alves", "Jelle Scholtalbers", "Bio-IT", "Community", "Mattermost", "Chat", "Milestone"]
title: "1.000.000 posts in the Bio-IT EMBL chat"
omit_header_text: true
---

# Bio-IT EMBL chat reached 1M posts

[EMBL's Bio-IT chat][embl-chat], initiated in 2017 to facilitate internal real-time interactions between people at EMBL, has reached the milestone of 1.000.000 posts in the first days of February 2022.

At close to 5 years of service, and after a significant increase in adoption in the early days of the pandemic, the EMBL chat is now a very active platform with close to 600 channels and a daily exchange of over 2000 messages from over 330 active users.

As the platform richness and usefulness is made through contributions of all of you, help yourself and **join us**.

If you are an active EMBL employee you can access https://chat.embl.org and login with your EMBL credentials by following [these instructions](https://grp-bio-it.embl-community.io/blog/posts/2021-01-29-mattermost-embl-chat/).

You will be greeted by our **Welcome Bot** that will direct you to some of the most active channels on a wide range of topics.

## Who should I contact in case of issues?

[EMBL Bio-IT chat][embl-chat] is maintained by Jelle Scholtalbers and Renato Alves and support requests can be sent to gitadmins@embl.de

We look forward to see many more milestones being hit.

Renato Alves and Jelle Scholtalbers,  
EMBL's Chat team

Photo by [Ray Hennessy](https://unsplash.com/@rayhennessy)
on [Unsplash](https://unsplash.com/s/photos/skill?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

[embl-chat]: https://chat.embl.org
