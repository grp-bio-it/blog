---
date: 2021-10-29
description:
featured_image: "/images/2021-10-29/chaitanya-pillala-zDDdoYqQ64U-unsplash.jpg"
tags: ["Renato Alves", "Bio-IT", "mattermost", "chat", "emoji", "hapiness", "enhancer"]
title: "Bio-IT Happiness enhancer badge"
omit_header_text: true
---

Over the years, the [Bio-IT EMBL Chat]({{< ref "2021-01-29-mattermost-embl-chat" >}}) has seen an ever increasing list of cute and bouncy happy emoji, dancing cats, parrots, blobs and memes.

We are always delighted to see how people customize the space to feel more fun and cheerful and so, to celebrate and recognize this contribution, we have created the **Happiness enhancer** badge that will be granted to our most active *emoji* creators:

![Happiness enhancer badge](../../images/2021-10-29/happiness-enhancer.png)

We love to see how you make this place so lively!
A big thanks and keep them smiley.

## Adding emojis

If you are reading this but don't know how to contribute an emoji, when picking an emoji, click the `Custom emoji` button as shown in the following image.

![Adding a custom emoji](../../images/2021-10-29/custom-emoji-01.png)

Then click the `Add Custom Emoji` button:

![Adding a custom emoji](../../images/2021-10-29/custom-emoji-02.png)

And follow the steps. The emoji name should start and end with `:`, for instance `:yoda-dance:`.

Have fun!  
Renato

---

Banner image by [Chaitanya Pillala @ Unsplash](https://unsplash.com/@chaiclikz).
