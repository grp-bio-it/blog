---
date: 2019-07-10
description:
featured_image: "images/ccmcr_header_john_jennings.jpeg"
tags: [Malvika Sharan, Community, Letter, Bio-IT]
title: "A Friendly Letter to Community Managers"
omit_header_text: true
---

*Dear Community Managers,*

*Have you been working on multiple projects with multiple groups of people? Have you ever (or every day) felt like that you are not specialised enough? Then these next 90 seconds are for you (because I just lost 30 seconds introducing myself).*

*You may think that you are less specialised than many in your community, but that’s mostly because you know too many people of specific expertise. Remember? your job comes with the responsibility of getting to know your community members.*

*You understand the work and needs of people in different departments, connect them with each other, and improve interoperability within your community.*

*You develop and maintain projects that are beneficial for your members. You create an inclusive space for the new and existing members to contribute to these projects. You value their time and effort and acknowledge them for their work.*

*You teach the same subject, over and over, to new learners, without finding that boring. And when you know that someone in your community needs training in a new skill, you learn those skills and bring that piece of new information for your members to learn.*

*You support your members in their work by offering your time, and often that means that your calendar is filled with meetings of sorts. Such involvement gives you clarity on how different resources, practices, knowledge, training, and social interactions impact different projects.*

*As a curious individual, you may have many specialties which we won't explore today (because I am running short of time), but as a community manager, your specialty is to combine multiple skills rather than achieving specialisation in one single skill.*

*So, if you are listening to this, and if you need to hear this - your work makes a difference - (sometimes just a little bit, like a missing colon symbol in the code of your learner).*

*Love,*

*Your Fellow Community Manager*

---------

I recently gave a lightning talk on the topic "Why being 'less specialised' is an important skill for the community managers" at the [CarpentryConnect Manchester](https://software.ac.uk/ccmcr19) on 25 June 2019. I wanted to give a proper presentation - but to fit the "1-slide-in-2-minutes"  rule, I wrote a short friendly letter to all the community managers (I am sure I could have gone on for longer if there was more time).

Several people who work as support staff related to my 2 minutes monologue, and asked me to share my letter with them (including a few people on Twitter). Assuming that many of you will also relate to it, I decided to post it online.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">.<a href="https://twitter.com/MalvikaSharan">@MalvikaSharan</a> used her <a href="https://twitter.com/hashtag/ccmcr19">#ccmcr19</a> lightning talk to read aloud a Letter to a Community Manager, which would have fitted in perfectly with the discussions we’ve had in <a href="https://twitter.com/hashtag/cefp2019">#cefp2019</a> this year. Thanks for the reassuring message, Malvika 🙌 <a href="https://t.co/VvZ7PI2BET">pic.twitter.com/VvZ7PI2BET</a></p>&mdash; Toby Hodges (@tbyhdgs) <a href="https://twitter.com/tbyhdgs/status/1143477343895339008">June 25, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Hosted by the [Software Sustainability Institute (SSI)](https://software.ac.uk/about) in Manchester and chaired by Aleks Nenadic (training head at SSI), this CarpentryConnect was very successful at bringing together people from different European countries who shared their passion for computational training and skill-transfer. Toby Hodges and I had an opportunity to contribute to the organisation of this first European [The Carpentries](https://carpentries.org/) (a volunteer-based community that teaches computational skills to researchers) event. As members of the organisation team, we offered our support wherever we could while planning, and led/co-led a few breakout sessions. This event allowed us to reconnect with our dear friends and colleagues in The Carpentries community, get to know more members, exchange ideas, and establish important collaborations. You can read the [blog posts](https://software.ac.uk/tags/carpentryconnect) and [tweets](https://twitter.com/search?q=%23ccmcr2019&src=typed_query&f=live) from many attendees including both of us for more details.

Cover Photo by [John Jennings on Unsplash](https://unsplash.com/photos/IcT8l8DDek8)
