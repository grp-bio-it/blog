---
date: 2022-01-11
description:
featured_image: "/images/2022-01-11/fotis-fotopoulos-DuHKoV44prg-unsplash.jpg"
tags: ["Renato Alves", "R", "RStudio", "Markdown", "SLURM", "Bio-IT", "EMBL"]
title: "Building R Markdown reports without RStudio"
omit_header_text: true
---

[RStudio][rstudio] is a powerful and user-friendly platform for developing and performing R based data analysis.
Coupled with [R Markdown][rmarkdown], one can easily generate visually rich reports that are one of, if not the, most used feature by the R community at EMBL to communicate results.

When the popular [GBCS][gbcs] [RStudio server][embl-rstudio] backed by the [large `seneca` server][seneca] is too busy, or if you need a convenient way to generate R Markdown reports in parallel or as part of elaborate pipelines,
you can use the following script:

```bash
#!/usr/bin/env bash

if [ "2" -gt "$#" ]; then
  echo "Usage: $0 script.Rmd path/to/output.html arg1=\'value1\' arg2=\'value2\' ..."
  exit 1
fi

function commas() { local IFS=","; echo "$*"; }

FILE="$1"
shift
OUTNAME="$(basename "$1")"
OUTDIR="$(dirname "$1")"
shift

echo "Running: R -e \"rmarkdown::render('$FILE',output_format='html_document',output_dir='${OUTDIR}',output_file='${OUTNAME}',params=list($(commas "$@")))\""
R -e "rmarkdown::render('$FILE',output_format='html_document',output_dir='${OUTDIR}',output_file='${OUTNAME}',params=list($(commas "$@")))"
```

saving the above content on a file called `Rmarkdown` and adding it to your shell's `$PATH` will allow you to execute

```sh
Rmarkdown my_analysis.Rmd output_folder/output.html arg1='value1' arg2='value2'
```

and build or knit R Markdown reports without RStudio.

To use the above command with the EMBL cluster, don't forget to load the appropriate R `module` (e.g. `module add R-bundle-Bioconductor-GBCS/3.11-foss-2020a-R-4.0.0`) beforehand.

See also [this approach from Francesco Tabaro](https://chat.embl.org/embl/pl/gte6cdnu6fyrura7xdkdkuxfoh) to build a report by submitting a SLURM job directly from R.

If you see errors related to `X11 display`, try adding `options(bitmapType='cairo')` to your script or `~/.Rprofile`, as described in the [GBCS wiki][cairo].

Happy reporting!  
Renato


Photo by [Fotis Fotopoulos](https://unsplash.com/@ffstop)
on [Unsplash](https://unsplash.com/s/photos/skill?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

[rstudio]: https://www.rstudio.com
[rmarkdown]: https://rmarkdown.rstudio.com
[embl-rstudio]: https://rstudio.embl.de
[gbcs]: https://gbcs.embl.de
[seneca]: https://gbservices.embl.de/Services/RStudio/
[cairo]: https://gbservices.embl.de/Services/RStudio/#cannot-convert-rmarkdown-to-pdfhtml

