---
date: 2023-05-04
description:
featured_image: "images/2022-09-15/embl_gitlab.png"
tags: ["Renato Alves", "Bio-IT", "GitLab", "SSH", "push", "pull", "clone"]
title: "Connecting to GitLab using SSH"
omit_header_text: true
---

When interacting with [GitLab at EMBL][git-embl] using [`git`][git], two protocols are available, [HTTPS][https] and [SSH][ssh].

The first is convenient for a quick interaction. However, it quickly gets annoying as it will ask for your credentials every time.
A more convenient and versatile option is [SSH][ssh] but it requires going through a few steps to configure it.

If incorrectly configured, you will likely see the error message: `Permission denied (public key). fatal: Could not read from remote repository`.

So lets get started with step-by-step instructions to create an [SSH][ssh] key and configure a new computer to clone or pull/push from [git.embl.de][git-embl] using the [SSH][ssh] protocol:

1. Open your terminal application on your computer or `Git Bash` (not `Git CMD`) if you are on Windows.

2. Type the following command to generate a new SSH key.

```
ssh-keygen -t ed25519 -C "your@email.com"
```

3. The command will prompt you to enter a file name where the key will be saved. The default name is `id_ed25519`, and it will be saved in the `~/.ssh` directory. Press Enter to accept the default name or enter a new name if you want.

If a file with the same name exists make sure you don't need it before confirming that it can be overwritten.

4. You will also be prompted to enter a passphrase. This is optional, so you can press `Enter` if you don't want to use a passphrase. However, setting a passphrase is a recommended practice for added security.

Upon confirming the previous step, two files will be generated; `id_ed25519` the private key, and `id_ed25519.pub` the `pub`lic key. The private key should never leave your computer. If anyone gains access to it they will be able to impersonate you via [SSH][ssh] on any service where you have registered your `pub`lic key. This includes [git.embl.de][git-embl].

5. After the key pair is generated, run the following command to add the private key to the [SSH agent][ssh-agent] that should be running in your computer:

```
ssh-add ~/.ssh/id_ed25519
```

If you see a `Identity added:` message, you were successful and can continue at step 6.

If instead you see the error:

```
Could not open a connection to your authentication agent.
```

You will need to start an [SSH agent][ssh-agent] first and then repeat the previous command.

```
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_ed25519
```

You should then see a  `Identity added:` message indicating that you were successful.

6. Once the key is added to the agent, you need to copy the public key to your [git.embl.de][git-embl] account. The easiest option is to open the file `~/.ssh/id_ed25519.pub` in your favourite text editor and copy its contents to the clipboard.

7. Open the [SSH keys page in git.embl.de](https://git.embl.de/-/user_settings/ssh_keys) in your favourite browser (Login to GitLab if necessary)

8. Click the `Add new key` button on the rightmost corner.

9. Paste the public key you copied earlier into the `Key` box (big input box with multiple lines).

10. Give your key a descriptive title/name so you can identify which computer generated and/or holds the private key. Adding something about the reason the key was added can also be useful to identify a key later on if necessary.

11. Select `Authentication & Signing` and pick an expiration date you consider adequate.

12. Click on the "Add Key" button to save the public key to your [git.embl.de][git-embl] account.

13. In a new directory with no sub-directories execute the following command:

```
git clone git@git.embl.de:your_username/your_repository.git
```

If no errors were shown your SSH key setup was successful.
If you are asked for a password, make sure the [SSH agent][ssh-agent] mentioned in point 5. is running and the key was added to it.

14. Finally, you can push and pull changes using the [SSH][ssh] protocol as well. Try it out with one of the two following commands:

```
git pull
git push
```

That's it! You have successfully created an SSH key and configured a new computer to push/pull to/from GitLab using the SSH protocol.

## Adding an existing directory to git.embl.de

If you happen to already have a directory on your computer that you would like to convert into a git repository, follow these extra steps.

15. `cd` into the directory and run `git init` to initialize the repository.

16. Tell git that the remote you would like to communicate with is the [git.embl.de][git-embl] server using:

```
git remote add origin git@git.embl.de:your_username/your_repository.git
```

If you see the error `error: remote origin already exists.` it's quite likely that you had another remote configured earlier.  
In this case you can run:

```
git remote -v
```

to list all configured remotes.

Last, if you want to point the `origin` remote to the [git.embl.de][git-embl] server, you can run the command:

```
git remote set-url origin git@git.embl.de:your_username/your_repository.git
```

17. After this you should be able to `git push` or `git pull` to/from [git.embl.de][git-embl].

If after point 17 you get an error indicating that changes were rejected because you are behind the remote, see [this article explaining what could be behind it][git-push-rejected].


Cheers,  
Renato

# Disclaimer

Chat logo by [unDraw][undraw].

[chat]: https://chat.embl.org
[bio-it-chat]: https://chat.embl.org/embl/channels/bio-it
[git-chat]: https://chat.embl.org/embl/channels/git
[git-embl]: https://git.embl.de
[git]: https://git-scm.com/
[ssh]: https://en.wikipedia.org/wiki/SSH
[ssh-agent]: https://smallstep.com/blog/ssh-agent-explained/
[https]: https://en.wikipedia.org/wiki/HTTPS
[undraw]: https://undraw.co
[git-push-rejected]: https://komodor.com/learn/how-to-fix-failed-to-push-some-refs-to-git-errors/
