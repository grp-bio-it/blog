---
title: "The Bio-IT training Catalogue"
date: 2023-01-17
tags: ["Lisanna Paladin", "Bio-IT", "training", "Emily Simons"]
omit_header_text: true
featured_image: "/images/2023-01-17/erol-ahmed-Y3KEBQlB1Zk-unsplash.jpg"
---

# We want you(r training requests)!

New year, new resolutions. It has always been central in the Bio-IT's mission to plan and deliver a **curriculum of 
training courses** designed on the **needs and wishes of the (biocomputational) community at EMBL**. From this year, we 
have a tool to collect them in a systematic way: the 
[Bio-IT training catalogue](https://catalogue.bio-it.embl.de/courses/catalogue/). 

The platform was initiated, and developed to an advanced state, from Bio-IT's summer intern Emily Simons, who joined
us through the [DAAD RISE project](https://www.daad.de/rise/en/). She designed a **database** now hosting data about the 
**Bio-IT courses that happened between 2014 and 2022**. Each *course* (course type) might have happened multiple times, 
each instance of a course is defined in the catalogue as an *event*. Courses are further classified based on the topics 
that they covered, the skills that they taught, the number of hours spent on each, 
and details about the audience, trainers, location, timeframe and format. In 
addition, each event is linked to the Bio-IT summary and training materials when available. The user interface, also 
largely developed by Emily, provides a way to visualise this data. The homepage features a list of all the 2014-2022 
courses that can be filtered using the window on top (see the figure below). Each course and event title links to a page
featuring a short description of the course, the timeline of its events and number of hours spend on the
[Bio-IT competencies](https://grp-bio-it.embl-community.io/blog/posts/2021-11-04-competencies/) (see the figure below). 

![Catalogue Home Page](../../images/2023-01-17/Bio-IT_catalogue.png)

We encourage you all to explore the platform! For trainees and in general all the Bio-IT community, there are two 
specific features of the platform that we would like to highlight:
- **The courses are linked to their training materials**, when available. This would allow you not only to have a 
clearer understanding of what the course is about, but also to consult them at your own pace, for asynchronous trainign.
- From the course page, you can **express your interest** in us organising a new event of each course. You will just need to 
fill a very quick form. We encourage your extensive usage of this feature, to **design this year's training programme in line
with your needs and desires**. 

![Catalogue Home Page](../../images/2023-01-17/Interest-button.png)

### Expressing interest

See for example how to express interest for a future **Introduction to R** course.
Start by [searching for this course](https://catalogue.bio-it.embl.de/courses/catalogue/)
and follow the steps in this animation.

![Expressing interest Introduction to R](../../images/2023-01-17/expressing_interest.gif)

*Photo by [erol](https://unsplash.com/@erol) on [Unsplash](https://unsplash.com/)*
