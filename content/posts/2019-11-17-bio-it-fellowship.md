---
date: 2019-11-17
description:
featured_image: "https://images.unsplash.com/photo-1572844980501-10a1fcb2587f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1651&q=80"
tags: ["Malvika Sharan", "Bio-IT", "Community", "Bio-IT Fellowship"]
title: "Launching the Bio-IT Outreach & Travel Fellowship program 2020"
omit_header_text: true
---

*Post by Malvika Sharan on behalf of the Bio-IT Team*

To express our appreciation and gratitude for your contribution to the [Bio-IT project](http://bio-it.embl.de/) and further support your involvement in such activities within and outside EMBL, we are launching the [Bio-IT Outreach & Travel Fellowship program 2020](https://bio-it.embl.de/bio-it-fellowship-2020/). 

Any EMBL member who has actively led, facilitated, contributed to or participated in a Bio-IT event or project can request a fellowship of **up to 500 Euros** to attend or organise an event in 2020 by [submitting an application](https://www.surveymonkey.de/r/bio-it-fellowship). 

Several projects within the Bio-IT community are led and facilitated by members who volunteer as workshop instructors, chairs of interest groups, maintainers of software infrastructures, organisers of events, and contributors of different projects. We are always grateful for the generosity and kindness that our members continue to show by helping to improve the quality of other’s work and the culture of our workplace.

With this project, we aim to provide small grants to these members towards attending or organising conferences, workshops and events that may not be directly related to their research but will help them build their skill-sets, grow professionally and build their research-related or other technical skills. Such opportunities are important for early-stage researchers and support-staff members for exchanging ideas with others, highlighting and promoting their work, expanding their networks and staying up to date in their fields of interest. 

The application can be submitted on a rolling basis, and there is **no defined deadline**. The decision regarding your application will be made within 2-3 weeks after receiving your application based on the availability of the Bio-IT coordinators, who will be reviewing your applications.

Though intended for the events in 2020, applications for this fellowship can already be submitted. Please find the complete information on [this page](https://bio-it.embl.de/bio-it-fellowship-2020/) and reach out to me and Toby for more details by emailing [bio-it@embl.de](mailto:bio-it@embl.de).

*Cover photo by [@portuguesegravityl](https://unsplash.com/photos/G0Pjh3pfuqE) on [Unsplash](https://unsplash.com)*
