---
date: 2024-01-24
description:
featured_image: ""
tags: ["Lisanna Paladin", "Bio-IT", "BioNT", "The Carpentries", "community event"]
title: "CarpentryConnect at EMBL in 2024!"
omit_header_text: true
---

Announcing a community event in EMBL Heidelberg, 12-14 November 2024.

# Join us for CarpentryConnect Heidelberg 2024 at EMBL!

More information at: [carpentries.org/blog/2024/01/announcing-cchd24](https://carpentries.org/blog/2024/01/announcing-cchd24/)

Bio-IT welcomes you to a community event, that we are co-organising with the project [BioNT](https://grp-bio-it.embl-community.io/blog/posts/2022-08-31-bio-nt-funded/) and [The Carpentries](https://carpentries.org/). 

We will talk about and act on how to connect people through training across sectors (Academia and Industry, as well as job seekers), how to enhance the impact of training activities, and how to increase diversity within our training communities. 

You will be able to [register to the event later](https://biont-training.eu/event-details/CarpentryConnect2024), keep it on your radar!
