---
date: 2020-09-10
description:
featured_image: "images/2020-09-10/gitlab_slack_at_embl.png"
tags: ["Renato Alves", "Christian Arnold", "Bio-IT", "Community", "Slack", "GitLab", "Integration", "Chat"]
title: "Connecting GitLab at EMBL and Slack"
omit_header_text: true
---

This quick tutorial shows you how to connect [GitLab at EMBL][embl-gitlab] with a Slack workspace.
This can be very useful when you want to stay informed about activities in your repository without having to check your emails all the time - in fact, you may decide to disable email notifications for the repository because Slack notifications are quicker to see, manage and act upon!

This tutorial also shows you the power of so-called [webhooks][gitlab-webhook]! Webhooks are a very powerful technique to connect different services with one another via automated notifications.
If you haven't heard about this concept, you can find nice introductions with illustrations [here](https://snipcart.com/blog/what-are-webhooks-explained-example) or [here](https://www.getvero.com/resources/webhooks/).

So let's start to connect EMBL Gitlab and Slack!

## Setting up Slack

We start with Slack!

First, decide in which channel the automated notifications should appear in.
Either use an existing channel (public or private) or, as in my case, create a new repository-specific channel that exists only for the purpose of receiving automated notifications.
For the following example, we call the channel `gitlab_integration`.


You then need to install a Slack App called [**Incoming WebHooks**][incoming-webhooks] in your workspace. Click **Add to Slack**

![](../../images/2020-09-10/upload_21e7c1dec6560f890bf91ea05ab57ce5.png)

and specify the Slack channel that shall be used for the integration (`gitlab_integration`).

![](../../images/2020-09-10/upload_5c97693cb441f3587227256c05896559.png)

Finally, hit the **Add Incoming WebHooks Integration** button and you are set.
If all goes well, you should now see a **Webhook URL** field.
Make sure to copy this address, which you will need in the next step.

![](../../images/2020-09-10/upload_7a499c626650be7ece5890c5833d44b9.png)


## Setting up Gitlab

Now let's configure Gitlab!

Go to the landing page of the Gitlab repository you want to add the integration to, navigate to **Settings > Integrations > Slack notifications**.

Now, select all triggers you want to receive automated notifications for and enter the exact name of the Slack channel this should be sent to (here: `gitlab_integration`).
Lastly, paste the webhook URL from Slack in the **Webhook** field and select for an arbitrary name that should be displayed in Slack for incoming messages.
I choose **automated_notifications** to make it clear this was a message that was automatically generated.

![](../../images/2020-09-10/upload_7ed746d7b3397c7a14fa2694fad7e0c7.png)

Hit the **Test settings and save changes** button and you should be good to go!

## Test whether it works

After hitting the **Test settings and save changes** button, you should already have a new notification in your corresponding Slack channel.
If not, revise all settings. Now, if you create a new issue, for example, you should receive a new notification instantly.

![](../../images/2020-09-10/upload_4510f21b367b0f358480924ba160676f.png)

## Disabling the integration

If this integration didn't work the way you expected or you'd like to have it disabled, you need to undo the previous two steps.

### Slack

Re-visit the Slack App page [**Incoming WebHooks**][incoming-webhooks] where you will find an active configuration.

![](../../images/2020-09-10/upload_88a50fed63ab480597a54f124882dfc8.png)

Click the little pencil icon and in the top-right corner of the following page hit **Disable** or **Remove**.

![](../../images/2020-09-10/upload_222bc229f67ee52fad1bdf0e51be6e2e.png)

### GitLab

On Gitlab visit **Settings > Integrations > Slack notifications** once again and you will find a blue **Enable integration** button. Disable that and you are all set.

![](../../images/2020-09-10/upload_698b7ea95596144eb8416d020ff85271.png)

## Be productive

Hopefully, this helps you to stay (even more) productive.
Webhooks can be used in many other cases that support it.
You may want to disable email notifications now for the repository.


Let us know if something is unclear!  
Christian Arnold & Renato Alves


[embl-gitlab]: https://git.embl.de
[gitlab-webhook]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[incoming-webhooks]: https://slack.com/apps/A0F7XDUAZ-incoming-webhooks
