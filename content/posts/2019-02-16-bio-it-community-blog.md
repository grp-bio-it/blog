---
date: 2019-02-16T19:29:59+01:00
description:
featured_image: "images/fZ_0JXvH1il_rawpixel_unsplash.jpeg"
tags: []
title: "Bio-IT Community Blog"
omit_header_text: true
---

## Bio-IT Community Blog

*Disclaimer: This post is a summary of [about](https://grp-bio-it.embl-community.io/blog/about/) and [contribute](https://grp-bio-it.embl-community.io/blog/contribute/) pages of this website. So if you have read them, you should probably skip it! :)*

**Bio-IT Community Blog** is an Open Source project where you as a community member are the contributor of its contents. This project will be developed by me (*Hi! I am Malvika Sharan, a coordinator of EMBL Bio-IT*) to engage with the members of Bio-IT community at EMBL.

[Bio-IT](https://bio-it.embl.de) is a community initiative, which aims to build, support, and promote computational biology activity at EMBL Heidelberg. Bio-IT community members are scientists who share a varying level of interests and expertise in bioinformatics and help each other by sharing their skills, experience, and tools with others. The community has been growing since 2012 and has gained diverse members of different research interests who are bioinformatics learners, trainers, resource-developers, problem-solvers, decision-makers, knowledge-sharers, visitors and supporters.

## Creating more opportunities to participate

A large proportion of this community is biologists and novice bioinformaticians, who make an important part of our learner's community, but often don't know how they can contribute beyond their participation at our training events. In order to improve their participation and create more opportunities in the community, I am developing this online community blog supported by my participation at [Mozilla Open Leadership](https://foundation.mozilla.org/en/opportunity/mozilla-open-leaders/), cohort 7, Culture track.

This blogging infrastructure will create opportunities for our community members to demonstrate their work and share ideas with the community. We specifically want to encourage our learner of diverse experience and use this platform to demonstrate their scientific work and academic stories.

Interviews and open dialogues on relevant social issues with our community members will also be published and used in improving our equity strategy going forward.

## You can contribute too!

If you want to [contribute](https://grp-bio-it.embl-community.io/blog/contribute/) to the development of this project, [write me a message](https://twitter.com/malvikasharan).

Everyone who will **become an author** for this project will **receive a complimentary USB stick**.

Looking forward to reading your posts!

Malvika Sharan

EMBL Bio-IT Community

*Cover photo by [@rawpixel](https://unsplash.com/photos/fZ_0JXvH1il) on [Unsplash](https://unsplash.com)*
