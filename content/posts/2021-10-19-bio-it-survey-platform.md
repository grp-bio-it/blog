---
date: 2021-10-19
description:
featured_image: "/images/2021-10-19/we-hear-you-unsplash.jpg"
tags: ["Renato Alves", "Bio-IT", "survey", "LimeSurvey", "infrastructure"]
title: "Bio-IT forms & survey platform"
omit_header_text: true
---

Back in early 2020 the Bio-IT project took another step towards ensuring data privacy.
Coupled with learning that different groups at EMBL had multiple independent subscriptions to a broad range of survey platforms, the Bio-IT project **setup [LimeSurvey][survey] locally and gained another powerful and reliable service**.

![](../../images/2021-10-19/landing.png)

Since then, [this platform][survey] has been used to collect feedback from **hundreds of workshop attendees**, inquire [**EMBL's Python User Group (EPUG)**](https://bio-it.embl.de/embl-python-user-group/) and [**EMBL's R User Group**](https://bio-it.embl.de/emblr/) communities, and to run **several internal surveys**.

With over a year of reliable service, this platform is accessible and **open to anyone at EMBL**.
To use it simply head over to [survey.bio-it.embl.de][survey] and **login using your EMBL credentials**.

![](../../images/2021-10-19/login.png)

If you are not physically at EMBL, for **security reasons**, you will need an **active VPN connection** to login to the management interface.
Failure to do so will result in:

![](../../images/2021-10-19/vpn-needed.png)

Once logged in, you will be greeted by the familiar EMBL colors, the possiblity to create a new survey from scratch, or use an existing survey as template.

![](../../images/2021-10-19/dashboard.png)

You will then be able to create your surveys using a **wide range of question formats**, including:

* Text (short, long)
* Single and multiple choice options
* Drop-down selections
* Scale, ranking and multi-dimentional arrays
* [**and more, many more**](https://manual.limesurvey.org/Question_types)

You can customize the type of question in the **big green button in the edit question** section:

![](../../images/2021-10-19/survey-edit-question-edit.png)

Need a survey with **questions that should only be shown when certain conditions are met**? We have your back!  
The platform includes a powerful engine that allows creating **complex conditional workflows** hiding or showing individual or entire groups of questions.

![](../../images/2021-10-19/survey-edit-conditional.png)

Want to survey a **pre-defined list of participants**? We got you covered too.  
In this case, announcement and reminder e-mails, user tokens and participation reports can be entirely managed through the platform.

![](../../images/2021-10-19/survey-edit-participants.png)
![](../../images/2021-10-19/survey-edit-email.png)

What about **plots and statistics**? Hehe, you are funny!  
There are multiple flavors, both **interactive**, in the survey results section, and in a **PDF report**.
Check some of them in the blog entry about the [survey we ran in the EPUG community](../2021-10-18-epug-community-discussion/).

Oh and if you are wondering what do participants see, have a look yourself at how **shiny the EMBL theme looks like**!

![](../../images/2021-10-19/survey-final-question.png)

In short, everything you can expect from a powerful survey platform is most certainly covered.

All of these features and options do bring some complexity so take your time exploring and, if you find this platform useful, [let us know](mailto:bio-it@embl.de).
We love to hear when our tools are making a difference and even more when you have suggestions about them!

Banner image by [Jon Tyson @ Unsplash](https://unsplash.com/photos/vVSleEYPSGY).

[survey]: https://survey.bio-it.embl.de
