---
date: 2022-09-15
description:
featured_image: "images/2022-09-15/embl_gitlab.png"
tags: ["Renato Alves", "Bio-IT", "Community", "GitLab", "LFS"]
title: "File size limits on git.embl.de and promoting Git LFS"
omit_header_text: true
---

Over the years we have been promoting [Git LFS][git-lfs] at [git.embl.de][git-embl].
With the move to an S3 backed storage, LFS is now the supported way to store large files in our platform.

## New limit on git push

In order to enforce the use of LFS over plain git for large files, as of 2022-09-15 we now reject pushes larger than 100Mb.

## Introducing git LFS

[Git LFS][git-lfs] is a solution to store large files in Git in a more efficient manner.
Git LFS is an extension to git and is not bundled in a regular installation. You will need to install it separately.

Please refer to [Git LFS][git-lfs] for download and installation instructions.
Once installed follow the instructions under the **Getting started** section.

If you have setup SSH-key based authentication, use of Git LFS with Bio-IT GitLab should not require any additional configuration.

If you encounter any problems, email us at ``gitadmins@embl.de`` or message us over the [EMBL chat][chat] on either the [Bio-IT][bio-it-chat] or [Git][git-chat] channels.

Cheers,
Renato

# Disclaimer

Chat logo by [unDraw][undraw].

[chat]: https://chat.embl.org
[bio-it-chat]: https://chat.embl.org/embl/channels/bio-it
[git-chat]: https://chat.embl.org/embl/channels/git
[git-embl]: https://git.embl.de
[git-lfs]: https://git-lfs.github.com
[undraw]: https://undraw.co
