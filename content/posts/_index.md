---
title: Posts
omit_header_text: true
featured_image: "images/alexisrbrown_unsplash.jpeg"
description:
type: page
date: 2020-09-07T18:20:00+01:00
---

## Bio-IT Community Blog


This project aims to offer a platform for sharing ideas, resources, tools & techniques, interesting publications, interviews and stories from all our community members.

*Cover photo by [@alexisbrown](https://unsplash.com/photos/omeaHbEFlN4) on [Unsplash](https://unsplash.com)*
