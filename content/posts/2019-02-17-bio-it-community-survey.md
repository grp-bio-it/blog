---
date: 2019-02-17T14:19:45+01:00
description:
featured_image: "images/oD21yO2uaFM_rawpixel_unsplash.jpeg"
tags: [survey, community, announcement, bio-it post]
title: "Community assessment survey"
omit_header_text: true
---

## Participate in our community assessment survey!!

This year we want to focus on the sustainability of this community and involve more voices of its members in shaping Bio-IT equity and support strategy. As an effort in this direction, we invite you to participate in our [community assessment survey](https://www.surveymonkey.de/r/bio-it-assessment-2019). **Your opinion matters to us**.

Many of you have attended one or several Bio-IT training and networking events organized by Bio-IT coordinators and several community members. We have enjoyed teaching you and having you as a part of our learner's community, but we want to hear more from you through this survey.

The survey is an opportunity for ongoing improvement work by identifying your needs as an individual member for training, consulting and other bioinformatics resources. Your responses will help us evaluate our equity strategies and involve our members who are currently underrepresented.

## Why should you participate?

With this survey, we will evaluate the impact of our training and social activities, and develop new community support strategies by offering exactly what you need. And, we are all ears whenever you want to talk to us about bioinformatics support or your involvement in this project.

## ...so participate!

So, if you have ever attended an event or plan to attend one in the future, please take 3 minutes to share with us **how we can improve your participation going forward**.

There is a number of ways you can actively engage with the Bio-IT community, see [this post](https://bio-it.embl.de/get-involved-in-2019/) for more information.

Please feel free to connect with us by emailing [bio-it@embl.de](mailto:bio-it@embl.de).

Best wishes,

Malvika & Toby

Coordinators of Bio-IT Project

*Cover photo by [@rawpixel](https://unsplash.com/photos/oD21yO2uaFM) on [Unsplash](https://unsplash.com)*
