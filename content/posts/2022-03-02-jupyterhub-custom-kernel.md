---
date: 2022-03-02
description:
featured_image: "images/2022-03-02/mockup-graphics-QOpX1O-Mcew-unsplash.jpg"
tags: ["Renato Alves", "Bio-IT", "Community", "JupyterHub", "Kernel", "Jupyter"]
title: "Adding and removing custom kernels to EMBL's JupyterHub"
omit_header_text: true
---

# Jupyter kernels

[Jupyter](https://jupyter.org/) is a server but also an ecosystem of tools for data exploration.  
[JupyterLab](https://jupyterlab.readthedocs.io) is a web interface to notebooks which can be used to document, visualize and perform computations.  
Computations in Jupyter are performed by a kernel, a separate component that understands how to perform calculations on request and return the result back to Jupyter.  

As such, a single JupyterLab installation can have multiple kernels with different configurations for different needs.
One can have a kernel for Python 3.8, another for 3.9 or 3.10, or even kernels from different languages such as R, Julia, or [many others](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels).

## Adding a kernel

### Creating a new environment

First start by creating a new environment with the software you need.  

```sh
conda create --prefix $HOME/conda-py310 python==3.10 jupyter seaborn
source activate $HOME/conda-py310
```

In this case, `seaborn`. `jupyter` should also be installed so we can register the kernel with jupyterlab.

### Installing/Removing the kernel

```sh
ipython kernel install --name "py-3.10-local" --user
```

We can then confirm that the kernel was installed by issuing the following command:

```sh
jupyter kernelspec list
```

that will list all registered kernels:

```
Available kernels:
  py-3.10-local    /home/ralves/.local/share/jupyter/kernels/py-3.10-local
  python3          /home/ralves/conda-py310/share/jupyter/kernels/python3
```

If at any point we want to remove the kernel, we can use the listed name to remove it.
For instance if we wanted to remove `py-3.10-local` that we added above we would:

```sh
jupyter kernelspec remove py-3.10-local
```

To confirm you can execute:

```sh
jupyter kernelspec list
```
that should list all but the removed kernel.
```
Available kernels:
  python3          /home/ralves/conda-py310/share/jupyter/kernels/python3
```

If you then navigate to the dashboard of jupyterlab, what before looked like:

![before](../../images/2022-03-02/initial_kernels_.png)

will then become after a few minutes. If it still doesn't appear, refresh your browser.

![after](../../images/2022-03-02/added_kernel.png)


## Other languages

As the jupyter ecosystem supports many languages, you can also install them on the EMBL jupyterhub system.  
If you wanted to install typescript support, you need to install the `itypescript` kernel written for node.js 

```sh
conda create --prefix /home/ralves/conda-tjs "typescript=3.6"
npm install -g itypescript
```

and then register it with jupyter

```sh
its --install=local --user
```

And like above, the new kernel should appear after a few minutes.


## Who should I contact in case of issues?

If you encounter any issues let us know in the [EMBL Bio-IT chat and channel](https://chat.embl.org).

Renato Alves,  
EMBL's JupyterDev team

Photo by [Mockup Graphics](https://unsplash.com/@mockupgraphics)
on [Unsplash](https://unsplash.com/s/photos/skill?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
