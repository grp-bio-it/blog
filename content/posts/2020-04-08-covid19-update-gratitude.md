---
date: 2020-04-08
description:
featured_image: 
tags: ["Toby Hodges", "Bio-IT", "Community", "Online Training", "Online Support", "Chat", "Gratitude"]
title: "Update on Bio-IT Support During Covid-19 Outbreak"
omit_header_text: true
---

A lot has happened in the past few weeks,
as everyone at EMBL has been adapting to
working from home.
It's been a busy time for the Bio-IT community:
as computational researchers and support staff,
most of our community members are able to continue working on most of their normal tasks/research,
the only major change being that they do that from home.
Many such members are facing additional challenges, though,
from struggling with reduced Internet connection speeds
to balancing their work tasks with the need 
to care for children and other family members.
So we've been delighted to see the enthusiasm with which
the community has come together online to 
meet the challenges these lockdowns pose to our work
and to support other members who are less able to keep working "as usual"
while the sites are closed.

[In our first post on this topic](https://grp-bio-it.embl-community.io/blog/posts/2020-03-13-bio-it-activity-online-covid-19/),
published as EMBL sites were beginning to close their doors,
I outlined how existing Bio-IT resources and support could be accessed 
and might help keep our community connected while Covid-19 restrictions are in place.
We've got a few updates to give in this post, 
and plenty of people to thank for helping to maintain and grow
support for computational research at EMBL during this difficult time.

### [`chat.embl.org`](https://chat.embl.org/) Update

In the previous post, 
we recommended the Bio-IT community's Mattermost chat system,
at [chat.embl.org](https://chat.embl.org).
Since the Covid-19 lockdowns began,
we've seen a huge increase in message traffic on the system,
and we're delighted to say that the platform is now accessible
also to EMBL-EBI members,
as long as they're logged into their institutional VPN.
We're extremely grateful to our colleagues
in EMBL IT Services and especially to Jelle Scholtalbers,
of the Genome Biology Computational Support team,
for their work to make this possible. 
It's great to have the whole community connected like this.

### Online Training

So far we've been able to run two introductory courses for remote participants
since the site closures began. 
These were the first Bio-IT workshops to welcome at least one participant
from each of EMBL's six sites, 
as well as external participants joining via the [de.NBI](https://www.denbi.de) network.
We were satisfied with how the courses went,
particularly given the short time we had to prepare to teach them online for the first time.
We'll write up our reflections on the experience of teaching by video conference
in a separate post soon.
For now, special thanks must go to Supriya Khedkar of the [Bork Group](https://www.embl.de/research/units/scb/bork/)
for meeting the challenge of teaching her first course under such unusual circumstances
with such style and positivity.

The next Bio-IT course to run online will be an [Introduction to R](https://bio-it.embl.de/events/introduction-to-r/),
to be taught by Thea Van Rossum (also of the [Bork Group](https://www.embl.de/research/units/scb/bork/))
and Florian Huber ([Typas](https://www.embl.de/research/units/genome_biology/typas/)/[Beltrao](https://www.ebi.ac.uk/research/beltrao) Groups)
on four consecutive mornings 5-8 May.

### Online Events Calendar

As many EMBL members aren't able to continue their regular work or research,
opportunities to learn new skills, 
connect with others,
and discuss science 
online are vital to the health of the organisation.
Colleagues from many different parts of EMBL have been quick to
adapt and start providing various different online activities.
The speed with which we were all able to establish a [shared calendar](https://bio-it.embl.de/), 
as a single point of reference for events, training, and other activities
taking place online,
is a testament to the positive and proactive attitude of the whole
EMBL community.
And another shout out should go to the EMBL IT Services team,
who were able to engineer a solution to provide access to the calendar
to members at EMBL-EBI, 
who normally can't access the shared calendars available
to the other five sites.

We've embedded the calendar [on the front page of the Bio-IT portal](https://bio-it.embl.de/) -
the list of events currently includes 
scientific and non-scientific training courses, 
webinars and online lectures,
statistics book club sessions (more info below),
drop-in and consulting sessions,
and journal clubs.
There's also a link to our [list of recommended online learning resources](https://bio-it.embl.de/online-learning/).

### MSMB Book Club

In the days before the EMBL Heidelberg site was closed,
Eva Geissen of the [Centre for Biological Modelling](https://bio-it.embl.de/centres/cbm/)
had the great idea of establishing a book club to work through
[Modern Statistics for Modern Biology](https://www.huber.embl.de/msmb/),
by Wolfgang Huber and Susan Holmes.

After some hasty planning to shift the club online,
the first session took place in late March.
Wolfgang Huber, research group leader at EMBL and co-author of the book,
was kind enough to spare time to join that session,
and hopes to be able to continue to drop by for some of the subsequent discussions too.
So far, the club has been meeting weekly,
with each session providing a platform for discussing one chapter of the book.
Eva's idea was popular -
we had ~70 people on the first call -
so we've been making extensive use of Zoom's breakout rooms feature
to split into sub-groups with a view of encouraging every member to get involved
in the discussion.
We've already made some tweaks to the format and,
with so many other things going on right now,
I've been finding it difficult to properly read the entire chapter each week.
But it feels like I'm not alone in the group and other members 
are also drawing motivation from the sessions,
to continue working through the material when they might otherwise be
distracted by more immediate demands of their attention.

We're fortunate too that Wolfgang and Susan,
the authors of the book,
have begun a series of online lectures and labs 
(via the [Stanford-EMBL Life Science Alliance](https://www.embl.de/LifeScienceAlliance/start/))
to accompany the book.
It's a fantastic opportunity for our club members to consolidate what they are learning from reading the text.
Many thanks to Eva for coming up with the idea and driving the organisation of the book club, 
and to the other member of the planning team, 
Matt Rogon 
of the [Centre for Biomolecular Network Analysis](https://bio-it.embl.de/centres/cbna/),
for setting up an RStudio Cloud environment
for the club to work in.

### What's next?

We've got plans for more online activities and other ways
in which we can continue to connect the EMBL Bio-IT community
while we all try to weather the storm of Covid-19.
We'll continue to post here when we have further updates,
and you can always reach out to us whenever you like.
In the meantime,
stay positive,
keep active,
and take care of those around you.
See you online!
