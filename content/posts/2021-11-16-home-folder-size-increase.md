---
date: 2021-11-16
description:
featured_image: "/images/2021-11-16/janko-ferlic-sfL_QOnmy00-unsplash.jpg"
tags: ["Renato Alves", "Bio-IT", "EMBL", "cluster", "home", "quota"]
title: "EMBL cluster home folder quota increased to 50GB"
omit_header_text: true
---

Over the years, the use of symlinks and `/g` group shares became a *de facto* standard and recommended practice to workaround the limited storage capacity of the home folder in the EMBL cluster.
While functional, these workarounds were unfriendly to beginners and suffered from intermittent problems associated with the overload and automatic cleaning procedures of shared drives and/or `/scratch`, 

Thanks to discussions and efforts following from a recent Bio-IT [taskforce meetings](https://pad.bio-it.embl.de/t0NtubmnSGm2-wuHu4eWXQ), this quota has now been expanded to 50 GB.
You will now be able to avoid the above mentioned workarounds and install your virtual environments, R packages, etc. without having to worry about running out of space.

As with all other shared resources, please use the space reasonably and continue using primarily `/scratch` for intensive I/O.
In case of doubt about how much space you are using at any given time, the command `quota` will provide the answer.

Renato

Photo by [Janko Ferlič](https://unsplash.com/@itfeelslikefilm)
on [Unsplash](https://unsplash.com/s/photos/skill?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
  
