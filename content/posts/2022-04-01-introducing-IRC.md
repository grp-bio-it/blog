---
date: 2022-04-01
description:
featured_image: ""
tags: ["Renato Alves", "Bio-IT", "Chat", "Communication", "Community", "IRC", "Mattermost", "April 1st"]
title: "IRC to replace Mattermost server"
omit_header_text: true
---

Following the feedback we obtained from all of you, we have decided to replace Mattermost, powering https://chat.embl.org, with an older technology, called [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat), following after [the successful example of other communities](https://libera.chat/).  
We hope that with this change you won't have to deal with the burden of finding the right emoji and meme for every occasion.  

You will also be able to invite your oldies if `/join #channel` and `/me` commands are too unfamiliar or confusing.

Renato Alves,  
from your friendly chat admin team
