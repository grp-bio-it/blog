---
date: 2023-02-07
description:
featured_image: "/images/2023-02-07/wynand-uys-4ZCA3xukIso-unsplash.jpg"
tags: ["Renato Alves", "Jelle Scholtalbers", "Bio-IT", "Community", "Mattermost", "GitLab", "Chat"]
title: "Where to find us on the EMBL chat"
omit_header_text: true
---

Ever since it's launch in 2017, [EMBL's Bio-IT chat][embl-chat] has been steadily growing in all directions, expanding the range of topics discussed, the number of [daily active users](../2022-02-08-mattermost-milestone/) and different functionalities, [some fun](../2022-10-04-trivia/), [some productive](../2021-02-15-bio-it-announce-chat/).

Since around this time last year, the chat has grown to have:
- over 1600 users of which 410 are active on a monthly basis
- 700 channels split between 120 public and 580 private
- 1.5 million posts, which is to say, [half million exchanged messages since last year](../2022-02-08-mattermost-milestone/)
- a little over 820 board cards, thanks to the adoption of the new [Task Management Boards](https://docs.mattermost.com/guides/boards.html) feature introduced last year

With such fast growth and many places to visit, it is normal to find yourself a little lost.

We hear you!! :mega: and we compiled a short list of channels you may want to visit depending on your interests:

## Computational research and data management support

- [Bio-IT](https://chat.embl.org/embl/channels/bio-it) :point_right: [Bio-IT](https://bio-it.embl.de/) channel for general bioinformatics, computational support
- [Bio-IT-announce](https://chat.embl.org/embl/channels/bio-it-announce) :point_right: [Bio-IT](https://bio-it.embl.de/) channel for course/workshop and Bio-IT activity announcements
- [cluster](https://chat.embl.org/embl/channels/cluster) :point_right: [EMBL Heidelberg HPC cluster](https://wiki.embl.de/cluster/Main_Page) support
- [MODIS (ex-GBCS)](https://chat.embl.org/embl/channels/gbcs) :point_right: [Multimodal Open Data Integration Support](https://www.embl.org/groups/modis/) channel
- [Mathematical Modelling](https://chat.embl.org/embl/channels/mathematical-modelling) :point_right: Channel to discuss modelling and interact with the modelling center channel
- [LabID (STOCKS)](https://chat.embl.org/embl/channels/stocks) :point_right: Support channel for [LabID (previously STOCKS)](https://stocks.embl.de/) EMBL's [MODIS (ex-GBCS)](https://chat.embl.org/embl/channels/gbcs) electronic lab and data management system
- [Folding / AlphaFold](https://chat.embl.org/embl/channels/folding) :point_right: A channel to discuss protein folding with AlphaFold and related technologies
- [How could be done better](https://chat.embl.org/embl/channels/how-could-be-done-better) :point_right: A channel to discuss scientific figures and other kinds of scientific output in a constructive way

## Programming and statistics

- [Git](https://chat.embl.org/embl/channels/git) :point_right: [Git](https://git-scm.com/) and [EMBL's GitLab](https://git.embl.de/)
- [Julia](https://chat.embl.org/embl/channels/julia) :point_right: [Julia](https://julialang.org/)
- [MachineLearning](https://chat.embl.org/embl/channels/machinelearning) :point_right: For classic and deep learning discussions
- [Python](https://chat.embl.org/embl/channels/python) :point_right: [Python](https://www.python.org/)
- [R\_and\_statistics](https://chat.embl.org/embl/channels/r_and_statistics) :point_right: [R](https://www.r-project.org/) and statistics
- [RStudio-Server](https://chat.embl.org/embl/channels/rstudio-server) :point_right: [RStudio](https://rstudio.embl.de/) EMBL server (by [GBCS](https://chat.embl.org/embl/channels/gbcs))

## Image analysis

- [ALMF](https://chat.embl.org/embl/channels/almf) :point_right: [Advanced Light Microscopy Facility](https://intranet.embl.de/scientific_services/core_facilities/almf/index.html) support channel
- [Image Analysis](https://chat.embl.org/embl/channels/image-analysis) :point_right: Image analysis discussion channel
- [Galaxy-Imaging](https://chat.embl.org/embl/channels/galaxy-imaging) :point_right: Support channel for image analysis in galaxy
- [Blender](https://chat.embl.org/embl/channels/blender) :point_right: Support channel for [Blender](https://www.blender.org/) a 3D computer graphics and image visualization software

## Workflow management and container/cloud technologies

- [Galaxy](https://chat.embl.org/embl/channels/galaxy) :point_right: [Galaxy@EMBL](https://galaxy.embl.de/) (by [GBCS](https://chat.embl.org/embl/channels/gbcs))
- [Nextflow](https://chat.embl.org/embl/channels/nextflow) :point_right: [Nextflow](https://www.nextflow.io/) workflow manager
- [Snakemake](https://chat.embl.org/embl/channels/snakemake) :point_right: [Snakemake](https://snakemake.readthedocs.io/) workflow manager
- [Singularity / Apptainer / Docker](https://chat.embl.org/embl/channels/singularity) :point_right: [Singularity](https://singularity.hpcng.org/) containers
- [Cloud Tech](https://chat.embl.org/embl/channels/cloud) :point_right: General discussion about cloud technologies
- [EMBL S3](https://chat.embl.org/embl/channels/embl-s3) :point_right: Discussion channel about [EMBL's S3 compatible storage](https://s3.embl.de/)

## Life at EMBL

- [Jobs](https://chat.embl.org/embl/channels/jobs) :point_right: Announce jobs and other opportunities in or outside of EMBL.
- [marketplace](https://chat.embl.org/embl/channels/marketplace) :point_right: General trading, buy&sell, giveaways, lost&found, ...
- [Staff Association](https://chat.embl.org/embl/channels/staff-association) :point_right: General channel for interaction with Staff Association and Staff Representatives.
- [Tax Support EMBL community](https://chat.embl.org/embl/channels/tax-support-embl-community) :point_right: Provide and seek answers concerning tax return filling and related questions.
- [EMBLAid-Heidelberg](https://chat.embl.org/embl/channels/emblaid-heidelberg) :point_right: EMBLAid channel for Heidelberg (see also other EMBLAid channels from other sites).
- [Trivia session](https://chat.embl.org/embl/channels/trivia-session) :point_right: Join us for some pub-quiz fun time every Thursday at 17:00 and test your general knowledge and typing speed on a fast paced 10-15 minutes trivia.


Renato Alves,  
EMBL's Chat team

Photo by [Wynand Uys](https://unsplash.com/@wynand_uys) on [Unsplash](https://unsplash.com/photos/4ZCA3xukIso)

[embl-chat]: https://chat.embl.org
