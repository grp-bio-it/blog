---
date: 2019-03-14
description:
featured_image: "images/_sharonmccutcheon.jpeg"
tags: [Katja Ovchinnikova, Ally Skills, inclusion, ally, social]
title: "Ally skills"
omit_header_text: true
---

*This post first appeared at [Katja's homepage](https://katja-hb.home.blog/2019/03/13/ally-skills/)*

#### Author: Katja Ovchinnikova
			 			
Last week, I co-led a session on ally skills with [Malvika Sharan](https://twitter.com/malvikasharan), [Sofya Mikhaleva](https://twitter.com/MikhalevaSofya) and [Paul Gerald Sanchez](https://twitter.com/poljiology) as a part of the [Women’s Day at the European Molecular Biology Laboratory](https://bio-it.embl.de/international-womens-day-2019/). The concept of the ally skills training was originally developed by [Valerie Aurora](http://valerieaurora.org/), a former Linux kernel developer. Valerie has designed extensive and well structured [materials](https://frameshiftconsulting.com/ally-skills-workshop) offering guidelines for learning and teaching how to step up and use our social advantages to support others in workplaces and communities. Watch Valerie’s talk on the topic.

Let me cite some of the definitions introduced by Valerie.

**Privilege**: an unearned advantage given to a person or a social group. **Oppression**: systemic, pervasive inequality present throughout society that benefits people with more privilege and harms those with fewer privileges. **Target**: someone who suffers from oppression. **Ally**: a member of a social group who enjoys some privilege and is working to understand their own privilege and end oppression.

Each of us can be a target or an ally depending on the context. For example, in the gender bias context I’m a target, whereas in the LGBTQ or racial context I’m privileged. Valerie is teaching how to recognize oppression and consciously act as an ally.

Most of the diversity and equality initiatives are aimed at changing the behaviour of targets (e.g. special scholarships, programs and mentioning for the under-represented social groups). Valerie points out that it’s efficient to focus on changing the behaviour of privileged people. Privileged people acting as allies have more power and influence as well as more time and energy as compared to targets. Moreover, they are not penalised for “diversity-valuing behaviour” and not accused of jealousy, but are rather seen as altruistic, giving, and kind.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">👍👍 to all organizers and attendees of the <a href="https://twitter.com/hashtag/AllySkills">#AllySkills</a> workshop <a href="https://twitter.com/embl">@embl</a>! And thanks <a href="https://twitter.com/vaurorapub">@vaurorapub</a> for creating it, sharing and empowering everyone <a href="https://t.co/IYolNhNHOS">pic.twitter.com/IYolNhNHOS</a></p>&mdash; Theodore Alexandrov (@thalexandrov) <a href="https://twitter.com/thalexandrov/status/1103949480091676672">March 8, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Following these ideas, we set up an 1 hour ally skills session. After an introduction of the basic concepts, terminology, and motivation, we did a short empathy exercise. Everyone was asked to stand up, look around and think of something nice about any 3 people in the room. Then the main part of the session started.

The participants were split into 6 groups from 4 to 6 people each. Each group received two scenarios to discuss. Here’s an example scenario:

> A woman you don’t know who is using a wheelchair is near your group at a conference. She is alone and looks like she would rather be talking to people.
What would you as an ally do?

Looks easy, right? Just invite her to join! But if we think deeper about it, questions arise. Does it make a difference that it’s a wheelchair user? Does it make a difference that it’s a woman? Can we even assume that it’s a woman without knowing the person? What is the best way to invite the person to join?

Before starting the discussion, the groups were asked to select a gatekeeper responsible for interrupting people who were speaking too much and asking people who weren’t talking as much if they wanted to speak. Our goal in this session was to simulate an ideal comfortable space for discussing most difficult topics with mutual respect so that every voice was heard. We suggested the participants to pay attention to the dynamics of the conversation and be mindful about it.

It often happens that targets feel uncomfortable speaking publicly. Although it’s totally fine to be an introvert and to have no need to share, nobody can represent people’s interests better than themselves. Therefore it’s important that every person has a chance to speak and be heard when they need it.

Since we had only 1 hour for the whole session, we could afford only 15 minutes for group discussions. This definitely proved to be not enough; the recommended time for an ally workshop is at least 3 hours. After the group discussions were over, each group briefly presented their solutions and we further discussed them all together. During this last part of the session organizers were giving further recommendations and pointing out interesting patterns. For example, in a few cases the groups were assuming that a protagonist of a scenario was a man, although the scenario was introduced in a gender-neutral way.

Overall, the participants were coming up with well thought out solutions and the atmosphere was very positive and productive. We hope that this experience will motivate them to act as allies in their work environments and communities.

