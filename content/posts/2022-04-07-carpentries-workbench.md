---
date: 2022-04-07
description:
featured_image: ""
tags: ["Lisanna Paladin", "Bio-IT", "training", "The Carpentries"]
title: "The Carpentries workbench, collaborative lesson development"
omit_header_text: true
---

# The Carpentries workbench

[The Carpentries](https://carpentries.org/) teach foundational coding and data science skills to researchers 
worldwide. The Carpentries is a community of people that gathers together with the aim to make technical skills
training effective, accessible and scalable. Most of the training happens in the form of short and very hands-on
training courses, collaboratively designed (and openly shared) in the community, and delivered by certified The 
Carpentries instructors (ream more on [how to become a certified instructor](https://carpentries.org/become-instructor/)) 
and multiple helpers, to ensure that all trainees are properly followed. 

Bio-IT implements The Carpentries model, and provides multiple Carpentries-inspired workshops (such as the [upcoming 
Python course](https://bio-it.embl.de/events/introduction-to-python-programming-6/)). We are following updates in the 
community and trying to implement the best practices developed there. Recently, The Carpentries presented the 
[Workbench](https://carpentries.github.io/sandpaper-docs/), for detailed information check their blog post 
[here](https://carpentries.org/blog/2022/01/live-lesson-infrastructure/). This platform allowed to completely redesign
the lesson development process, which is so far based on GitHub repositories 
([example](https://github.com/swcarpentry/python-novice-gapminder)) that through actions render the training materials
([example](https://swcarpentry.github.io/python-novice-gapminder/)). The [Workbench](https://carpentries.github.io/sandpaper-docs/)
should instead allow to separate the development of the training content from the tools that generate the webpages where they
are displayed. 

If this brief introduction got you interested in trying it, well... we are too! And The Carpentries just announced a
[Collaborative Lesson Development pilot](https://carpentries.org/blog/2022/04/lesson-development-training-pilot/) that 
will take place in June and July. The pilot is aimed at teams of 2-4 people who wish to collaborate on a new lesson... 
would you like to join forces and apply as a team? We are thinking about developing [a lesson comparing different 
Workflow Management Systems](https://lisanna.github.io/scientific-workflow-systems/), based on a [course that we run 
in February](https://bio-it.embl.de/events/overview-of-scientific-workflow-systems-workshop/), but any other idea can be
discussed. just let us know in the [Bio-IT chat channel](https://chat.embl.org/embl/channels/bio-it) what do you think! 
