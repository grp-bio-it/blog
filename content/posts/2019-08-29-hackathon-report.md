---
date: 2019-08-29
description:
featured_image: "images/hackathon_header_kaleidico.jpg"
tags: [Malvika Sharan, Toby Hodges, hackathon, annual, Bio-IT]
title: "Report from the first Bio-IT Hackathon (2019) and future plans"
omit_header_text: true
---

On 4 July 2019, we hosted the very first Bio-IT hackathon. We invited our community members from various research groups/departments a few months ahead of the event and asked them to think of projects that they could pitch for this hackathon. In order to provide a selection of topics, we encouraged project ideas ranging from developing technical tools to writing documents. We were successful at drawing [16 contributors](https://hackmd.io/79efcMTLSBmWzbmvxwg_AA) from different research and non-research groups to participate in this event.

![](../../images/2019_poster_1.png)

In the past, we have organised and participated in several hackathon-style events in different community spaces. These events aimed at achieving different collaborative outcomes such as developing functioning software to learning resources, publications, and web-pages. Through our experiences at these events we had concluded that, for a hackathon to be successful, the participants must be able to work in an environment that is inspiring and productive for everyone involved. With the motivation of providing this experience for others in our community, one goal that we set for the event was to gather project proposals that would be interesting and accessible to people with different expertise and interests in programming and technical topics. In this regard, we were very happy with the selection of projects pitched by several members a month before the hackathon. 8 different projects (check the [Issues](https://git.embl.de/grp-bio-it/hackathons/issues)) were proposed, which were both creative and useful for the community and catered to both our programmer (web scraping, R Shiny development, developing website infrastructure) and non-programmer (graphic design, tutorial writing, digital storytelling) community members. Historically, ‘hackathons’ in tech-spaces have not been the most inclusive events, therefore we paid attention in making sure that we personally reached out to individuals, especially women researchers who would bring diverse perspectives and distinct expertise, but may not consider to join us at the event.

Since we had never experimented with this format at EMBL, we were initially quite nervous about how successful the event would be. However, we were delighted to witness the high level of engagement among the participants who worked together in small teams and progressed in their projects over the course of the day. Thanks to our wonderful attendees for coming along and contributing to the following five Open projects: [visualising slurm dashboard for cluster usage](https://git.embl.de/pecar/slurm_dashboard) (R shiny app), [learning material image analysis in Python/Jython/Matlab](https://git.embl.de/grp-bio-it/image-analysis-with-python), [short tutorials for advanced Python learners](https://git.embl.de/grp-bio-it/mini_tutorial), [EMBL group directory](https://git.embl.de/fhuth/embl-group-directory), and [open-source visual library of scientific icons](https://resources.embl.org/login/) (see details [here](https://grp-bio-it.embl-community.io/hackathons/)).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Malvika did a great job of kicking off the day. We have four teams working on various projects, using <a href="https://twitter.com/hashtag/rstats">#rstats</a>, <a href="https://twitter.com/hashtag/python">#python</a>, <a href="https://twitter.com/gitlab">@gitlab</a>, and developing a library of graphics for talks, webpages, etc <a href="https://t.co/OyGcmgKXhy">https://t.co/OyGcmgKXhy</a> <a href="https://t.co/65bPKfC4iJ">pic.twitter.com/65bPKfC4iJ</a></p>&mdash; Toby Hodges (@tbyhdgs) <a href="https://twitter.com/tbyhdgs/status/1146716372157771777">July 4, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Here is a brief list of the tasks that went into organising this hackathon, and the lessons we learned from the post-event survey: 

**Logistics that we needed to take care of**

- Booking a venue with projector, white-board, movable tables and chairs, and enough room for everyone to move around
- Ordering coffee, snacks, drinks, and non-messy food (that accommodated all dietary requirements) for the entire day so that people could take a break whenever they wanted
- Arranging power sockets/strips, sticky notes, flip-charts, pens, and name tags for everyone
- Setting up a git repo, git page, shared HackMD document, registration page, and promotion materials (emails and posters) with all the required details
- Setting up an online call for project pitches a month in advance, recording them and sharing them with the notes on the repo for everyone who could not attend the call
- Putting a small presentation together introducing the format and a rough schedule to make sure that everyone knows what to expect on the day
- Introducing breaks in the program for each group to report back and share their progress with everyone
- Setting up a post-event survey with specific questions that could give insights into things that we did right and things we could do differently next time

**Post-event survey report**

We received 10 responses to our survey, of which 7 participants rated the event as good and 3 found it excellent. 

 ![](../../images/hack_new.png)

*Everyone indicated that they learned something new in the event.*

60% of the respondents thought that the event duration was too short, and 1 person thought that it was too long. 50% of the participants indicated that they will keep on working on the projects afterwards. The nature of a project or a particular challenge may affect peoples' engagement and the amount of time that they invest enthusiastically.

 ![](../../images/hack_members.png)

*60% of the respondent got to know at least one new member who they didn't know before.*

7 participants indicated that they will come back to this event if we organise it again, and the remaining respondents replied in 'maybe'.

 ![](../../images/hack_recommend.png)

*90% of the respondents indicated that they will recommend this event to others.*

You can see the complete survey report on the [GitLab repository](https://git.embl.de/grp-bio-it/hackathons/blob/master/2019/hackathon-survey-summary.pdf).

According to our attendees, these were the best thing about the hackathon:

> "Working together with different people than usual, working on something else than the day-to-day projects."

> "Opportunity to see how amazingly and quickly teams could work."

> "The possibility to meet and get to know more people from the Bio-IT community."

> "Organisation, team spirit"

> "A lot of different projects and people with different expertise."

> "Real-time exchange of ideas and suggestions"

**Based on the feedback, here are the things that we did right**

- Using collaborative document (HackMD) from the beginning
- Creating a Git repo to check the progress and allow others to contribute
- Inviting project ideas in advance to give enough time to people to choose their topics and get prepared to contribute
- Setting up a registration page to allow attendees to choose their projects of interests
- Reaching out actively to members who could contribute but hadn’t registered
- Creating a rough schedule and being ready to adapt (there was a staff assembly on the same day for which the announcement came out after we had planned the event)
- Reserving an on-campus location that was accessible yet slightly away from the usual workspace
- Organising stationery materials and ordering food and drink at the venue
- Ending the day with a relaxed BBQ event, which is also organised as the Bio-IT annual event

According to our participants, these are the things that we can do to make the next hackathon better:

> "More food / snacks :)"

> "Larger room with better connectivity and charging ports"

> "The Staff Assembly was an unfortunately timed distractor. Also, it would be a better use of time to encourage shorter, more to-the-point presentations."

> "As our project wasn't a Bio-IT one we didn't have any extra people to work with - maybe better advertisement would help if this would be the aim in future."

> "team leaders can be given some guidance on how to set team goals? better defined group and individual tasks would have been better. More time to work on projects, perhaps more time spread over a few afternoons."

> "More time to work on the projects (maybe make it a 2-day event)"

> "Make it longer. At least 2 days, with topics selected far before and small prizes at the end."

**Based on the suggestions, these are the things we will do differently next time**

- Book a bigger room as more people than expected showed up on the day, which we should encourage for the next time as well
- Order more coffee, and more food for the same reason as above (any leftover can be taken to the BBQ)
- Announce the event with more time to allow people to form groups to pitch an idea collaboratively and work together
- Save some time for the end to get a final report-out from each group and thank everyone
- Plan to kickoff this event an evening in advance where people can introduce their topics, set some goals, and come back the next day to directly work on the projects without losing time

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Now a much larger bbq. Thanks to <a href="https://twitter.com/tbyhdgs">@tbyhdgs</a> and <a href="https://twitter.com/ZellerGroup">@ZellerGroup</a> for arranging it and taking care of all dietary requirements 🙌🏽 <a href="https://t.co/0HgWsG5YyM">pic.twitter.com/0HgWsG5YyM</a></p>&mdash; Malvika Sharan (@MalvikaSharan) <a href="https://twitter.com/MalvikaSharan/status/1146825893240856576">July 4, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

**A few more ideas for the next year**

- Have a pre-event survey which could be useful to attract more people
- Think about low effort option to collect post-event survey as people would not want to do more work after a full day of the hackathon
- Reach out to more non-scientific members of EMBL for more project ideas and engagement
- Have a plan in place to track the progress of these projects afterward
- Have a real bell for merge requests from the contributors (an absolute necessity!)

We are quite encouraged by the outcome and we definitely want to add this to Bio-IT's annual traditions. However, for the next time, we would host this event under a different name such as 'CollaborationFest' or 'ContributionFest' (as used by Open Bioinformatics Foundation and Galaxy). The term 'hackathon' can be mistaken for an event meant for expert programmers or professional coders and may unintentionally become an exclusive place for only a small subset of the community. We believe that in addition to ensuring that this event is welcoming for everyone, we can also rename this event to something as accessible as 'Fest' and aim to attain participation from a wider and more diverse audience who may otherwise won't consider joining. As suggested in the feedback, we will start planning for next year with enough time in hand that can allow more people to propose projects and offer multiple options for everyone. So, if you are interested in getting involved in planning this event in 2020, and if you have ideas for either coding or non-coding projects that community members could work on, join us as a project leader, contributor, and/or organiser regardless of your interest or level of technical expertise. Please, feel free to reach out to the Bio-IT community coordinators by emailing bio-it@embl.de.

Finally, we want to express our gratitude to the members of our community who support our ideas and show up at Bio-IT events like this one. We really appreciate the time and effort they put in not only during such events but by contributing to other Bio-IT projects for months and years, which have made the work of others in our community easier. 

Thank you!

Malvika and Toby

Bio-IT community coordinators 

*Cover Photo by [Kaleidico on Unsplash](https://unsplash.com/photos/gVtJgTyi2iI)*
