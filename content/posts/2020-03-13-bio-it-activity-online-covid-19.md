---
date: 2020-03-13
description:
featured_image: 
tags: ["Toby Hodges", "Bio-IT", "Community", "Online Training", "Online Support", "Chat"]
title: "Accessing Bio-IT Activity and Support During Covid-19 Outbreak"
omit_header_text: true
---

Restrictions are increasingly being rolled out
with a view to slowing the spread of the novel Coronavirus.
So far, the EMBL sites in Rome and Barcelona have been closed 
and our colleagues at those stations are having to find ways to work from home (where possible).
Similarly, members at all other stations have been asked to work from home
whenever they can, 
and in-person events both large and small are being cancelled to reduce the risk
of viral transmission.

Everyone has a personal responsibility to do their part in slowing the spread of
disease and we are working towards this goal within the EMBL Bio-IT community as well.
As a computational research community, 
with the help of infrastructure and support provided by our colleagues in EMBL IT Support,
we're in the fortunate position that a lot of our work can be done remotely.
Regardless of whether your work is affected by the current restrictions or not, 
several of the resources provided by EMBL Bio-IT can help with this:

- The internal workplace **chat system** provided by the Bio-IT community, [chat.embl.org](https://chat.embl.org), is open to all EMBL members\* and accessible without the need for a VPN setup. You can use this system to communicate on open and private channels, and for direct, one-to-one messaging.
- Several **training courses**, organised by Bio-IT and the [EMBL Computational Centres](https://bio-it.embl.de/centres/), will be available to follow online. This includes our [Introduction to Regular Expressions](https://bio-it.embl.de/events/introduction-to-regular-expressions-2/) course taking place next week, and another course (topic TBC) taking place on 23 & 24 March. Check [our list of upcoming trainings](https://bio-it.embl.de/upcoming-courses/) for more details.
- Our [lesson development sprint](https://bio-it.embl.de/events/lesson-development-sprint-common-workflow-language-tutorial/) for a new Common Workflow Language tutorial will also now take place virtually. Anyone interested is welcome to join!
- Regarding **consulting** sessions: the regular weekly Bio-IT drop-in sessions will take place online for as long as Covid-19-related restrictions are in place. See [the relevant event pages on the Bio-IT Portal](https://bio-it.embl.de/drop-in/) for more details. In additon to these recurring weekly sessions, we and our colleagues in the EMBL Computational Centres are also available for consulting sessions on demand - use the contact details listed below to get in touch and arrange a suitable time.
- We work hard to ensure that all our **course material** from previous training events is [freely available online](https://bio-it.embl.de/course-materials/). If you're stuck at home and can't make progress with your research, why not use this opportunity to learn some computational skills? If you're working through any of our training material and get stuck/have questions, please don't hesitate to contact us!

In addition to these intial measures, 
we will continue to develop new ways to meet the needs of the community in meeting this challenge.
If you would like to speak with us about this,
or if you have ideas for how we and/or the wider community could support you
with the computational aspects of your research,
please feel free to contact us at [bio-it@embl.de](mailto:bio-it@embl.de).


\* EMBL-EBI members need to have credentials in the central LDAP system maintained by IT Support in Heidelberg