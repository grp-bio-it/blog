---
title: About
omit_header_text: true
featured_image: "images/BioIT_logo_about.png"
description:
type: page
date: 2019-02-15T10:00:00+01:00
menu:
  main: {}
---

Hello!

Thank you for visiting the [EMBL Bio-IT Community](https://bio-it.embl.de/) Blog.

**Bio-IT Community Blog** is an Open Source project where you, as a community member, are a contributor to its content.

**Short summary**

This project was originally developed by [Malvika Sharan](https://twitter.com/malvikasharan) and [Toby Hodges](https://twitter.com/tbyhdgs), and now [Renato Alves](https://bio-it.embl.de/renato-alves/), to engage with the members of Bio-IT community at EMBL.
Bio-IT is a community initiative, which aims to build, support, and promote computational biology activity.
Bio-IT community members are scientists who share varying level of interests and expertise in bioinformatics who help each other by sharing their skills, experience, and tools with others.

This community started at EMBL in 2011 unofficially, and in 2012 it was officially acknowledged as a valuable part of the organization.
The community has grown ever since and have gained diverse members from different research interests who are bioinformatics learners, trainers, resource-developers, problem-solvers, decision-makers, knowledge-sharers, visitors and supporters.
A large proportion of our community are biologists and early bioinformaticians (novice learners), who make an important part of our learners’ community.
While primarily based at EMBL Heidelberg, over the years, the community has expanded to include active members from all six EMBL sites.

If you are one of the above listed members, I am happy to welcome you as a new (or returning) member of this community.

**Aim of this project**

This community blog, was created to improve engagement and active participation from our community members.

This blogging infrastructure will create opportunities for our community members to demonstrate their work and share ideas with the community.
We specifically want to encourage our community to use this platform to demonstrate their scientific work and academic stories.

Interviews and open dialogues on relevant social issues with our community members will also be published and used in improving our equity strategy going forward.

If you want to [contribute](contribute/) to the development of this project, [write us an email](mailto:bio-it@embl.de).

Best wishes,

Renato Alves

on behalf of the EMBL Bio-IT Community ([https://bio-it.embl.de/](https://bio-it.embl.de/))

(content originally by Malvika Sharan)
