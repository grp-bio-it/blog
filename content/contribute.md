---
title: Contribute
omit_header_text: true
featured_image: "images/simonmaage_unsplash.jpeg"
description:
type: page
date: 2019-02-15T10:00:00+01:00
menu:
  main: {}
---

Would you like to contribute to this project?

### Here is how you can participate:

1. Contact [Renato Alves](mailto:bio-it@embl.de) to learn more about this project.
2. Share one or multiple contents. Here is a non-exhaustive list of types of content that can be published here.

  - *Bioinformatics materials*
      - a bioinformatics tutorial
      - a tool that you have developed
      - a technique that you think is cool
      - relevant publications (yours and others')
      - *anything tech* that changed your research

   - *Other relevant materials*
      - This blog post wants to encourage submissions related to your scientific and personal journey.
      - Posts on your technical skills and social experiences, your personal challenges, failures or success as scientists will be invaluable for us.

3. Create an issue to correct an error or add your post
4. Send a pull request to correct any error or add your post
5. Schedule a meeting with me to discuss your idea or getting your interview published

### Submitting posts:

Please use this [markdown template](https://git.embl.de/grp-bio-it/blog/blob/master/content/posts/2019-02-16-bio-it-community-blog.md) to write your post and submit via email, issue or pull request.

You can read more about the GitLab Pages and data protection [here](https://bio-it.embl.de/gitlab-pages/)



*Cover photo by [simonmaage](https://unsplash.com/photos/KTzZVDjUsXw) on [Unsplash](https://unsplash.com)*
